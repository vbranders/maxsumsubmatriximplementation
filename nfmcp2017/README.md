# This folder contains the code and data used to produce the results in the NFMCP 2017 paper.
# The code includes all implementations and a main class (Runner) to perform comparisons
# as well as the code used to generate datasets with fixed seed.
# 
# A analysis is performed by running src/main/scala/Runner.scala with appropriate arguments.
# The first argument determines which implementation to use to solve a specific problem.
# Other arguments are further passed to the class of the approach considered.
# 
# Real (Breast Cancer) data are stored in data/BRCA.
# The file data/BRCA/full_matrix_wnames_rank.tsv is directly obtained from the experiments by Le Van et al (2014).
# To avoid issues with storage size, all synthetic data are not generated. As the code is provided, appropriate
# use of the arguments allows to reproduce the same instances.
# For comparisons purposes, some examples are provided for each settings considered in the NFMCP paper.
#

