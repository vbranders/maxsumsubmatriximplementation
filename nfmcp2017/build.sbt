

lazy val root = (project in file(".")).
  settings(
      name := "maxSumSubMatrixNFMCP2017",
      scalaVersion := "2.11.4",
      javaOptions in run += "-Xmx4G",
      scalacOptions += "-feature",
      resolvers += "Oscar Snapshots" at "http://artifactory.info.ucl.ac.be/artifactory/libs-snapshot-local/",
      libraryDependencies += "oscar" %% "oscar-cp" % "4.0.0-SNAPSHOT" withSources(),
      fork in run := true)//or equivalently::: trapExit := false
