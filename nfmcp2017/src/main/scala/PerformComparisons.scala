import Solutions.BranchAndBound.MaxSumSubMatrixBranchAndBound
import Solutions.CP.{MaxSumSubMatrixCPLeVan, MaxSumSubMatrixCP}
import Solutions.MIP.MaxSumSubMatrixMIP
import utils.{FileReporter, JsonBuilder}

import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
  * Created by vbranders on 17/02/17.
  *
  */
object PerformComparisons extends App {



  val defaultChoice = 5
  val i = {if(args.length > 0 && args(0).length > 0) (args(0).toInt) else {defaultChoice}}
  val run = i match {
    case 0 => "BoundingFunction"
    case 1 => "SelectionStrategy"
    case 2 => "HeuristicAccuracy"
    case 3 => "Performances"
    case 4 => "ManyRepeats"
    case 5 => "Validation"
    case 6 => "GeneExpression"
  }
  println("Performing: " + run)
  val name = run
  val path = "data/paper/" + name + "/"
  val srcFile = path + "paths.txt"
  val parFile = path + "par.JSON"

  val doExp = true

  val names = run match {
    case "BoundingFunction" => Array("BB_UP", "BB_UP_naive")
    case "SelectionStrategy" => Array("BB_UP", "BB_OBJ", "BB_FRAC")
    case "HeuristicAccuracy" => Array("BB_FRAC", "CP_LNS", "CP_LNS_base")
    case "Performances" => Array("CP_LNS")//, "CP_LNS", "CP_LNS_base")//, "MIP")
    case "ManyRepeats" => Array("BB_UP", "BB_OBJ")//, "BB_FRAC_MEAN")
    case "Validation" => Array("CP_LNS")
    case "GeneExpression" => Array("CP_LNS")
  }

  var timeLimit = run match {
    case "BoundingFunction" => 0
    case "SelectionStrategy" => 10*60
    case "HeuristicAccuracy" => 10*60
    case "Performances" => 5
    case "ManyRepeats" => 15
    case "Validation" => 10*60
    case "GeneExpression" => 15
  }
  timeLimit = {if(args.length > 1 && args(1).length > 0) (args(1).toInt) else {timeLimit}}
  println("Time limit: " + timeLimit)

  for(file <- Source.fromFile(srcFile).getLines()){

    println("------------------------------------------------------------------------------------------------------------------------")

    if(doExp) {
      for (i <- 0 until names.length) {
        System.gc()
        println("*** > --------------- " + names(i))
        val b = names(i) match {
//          case "BB_UP" => MaxSumSubMatrixUP
//          case "BB_UP_naive" => MaxSumSubMatrixNaiveBound
//          case "BB_OBJ" => MaxSumSubMatrixOBJ
          case "BB_FRAC" => MaxSumSubMatrixBranchAndBound
//          case "BB_FRAC_REV" => MaxSumSubMatrixPositiveRatioRev
//          case "BB_FRAC_MEAN" => MaxSumSubMatrixPositiveRatioMean
          case "CP_LNS" => MaxSumSubMatrixCP
          case "CP_LNS_base" => MaxSumSubMatrixCPLeVan
          case "MIP" => MaxSumSubMatrixMIP
          case _ => {println("Error!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"); MaxSumSubMatrixBranchAndBound}
        }
        var t = new Thread {
          override def run() {
            b.main(Array(file, names(i), timeLimit.toString))
          }
        }
        t.start()
        t.join(1000 * timeLimit)
        val completed = !t.isAlive
        b.storeJson()
        println("")
        println(if (!completed) "Interrupted !" else "Completed !")
        t.interrupt()
        //t.stop()
        t = null
        println("----------------------------------------")
        System.gc()
      }

    }

  }

  var jsonRes: JsonBuilder = new JsonBuilder()
  jsonRes.add("metric", "time")
  var labels: ArrayBuffer[String] = new ArrayBuffer[String]()
  var instances = new ArrayBuffer[String]()
  for(file <- Source.fromFile(srcFile).getLines().map(_.replace("_matrix_norm.tsv", "_Parameters.JSON"))) {
    var lab = ""
    for(l <- Source.fromFile(file).getLines()) {
      if(l.contains("label")) {
        lab = l.split("\"")(3)
      }
    }
    if(lab.length > 0) {
      labels += lab
      labels = labels.distinct
      instances.append("[" + labels.indexOf(lab) + "]")
    }
  }
  jsonRes.add("labels", labels.toArray)
  jsonRes.insert("instances", "["+instances.toArray.mkString(",")+"]")

  var mapp: Map[String, Array[Array[String]]] = Map()
  var jsonData: JsonBuilder = new JsonBuilder()
  for(file <- Source.fromFile(srcFile).getLines()) {
    for(name <- names) {
      val f = file.replace("_matrix_norm.tsv", "_matrix_norm." + name + ".JSON")
      var n = ""
      var value = ""
      var value2 = ""
      var value3 = ""
      for(l <- Source.fromFile(f).getLines()) {
        if (l.contains("name")) {
          n = l.split("\"")(3)
        }
        if (l.contains("timeToBest")) {
          value = l.substring(l.lastIndexOf("\"")+3, l.indexOf(","))
        }
        if (l.contains("BestSolScoreToStop")) {
          value2 = l.substring(l.lastIndexOf("\"")+3, l.indexOf(","))
        }
        if (l.contains("timeToStop")) {
          value3 = l.substring(l.lastIndexOf("\"")+3, l.indexOf(","))
        }
      }
      if(!mapp.contains(n)){
        mapp = mapp + (n -> Array(Array[String](), Array[String]()))
      }
      mapp = mapp + (n -> Array(mapp(n)(0) ++ Array(value), mapp(n)(1) ++ Array(value2)))
    }
  }
  mapp.keys.foreach{ i =>
    var jsonTmp: JsonBuilder = new JsonBuilder()
    jsonTmp.insert("time", "["+mapp(i)(0).mkString(",")+"]")
    jsonTmp.insert("score", "["+mapp(i)(1).map(x => (x.toFloat)).mkString(",")+"]")
    jsonData.add(i, jsonTmp)
  }
  jsonRes.add("data", jsonData)
  new FileReporter(jsonRes.toString(), path + "res.JSON")


}
