package Solutions.BranchAndBound

import java.util.Calendar

import Solutions.Solution

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
  * Created by vbranders on 18/02/17.
  *
  */
object MaxSumSubMatrixBranchAndBound extends App with Solution {

  /**
    * Arg:
    *   (0): file path to matrix
    *   (1): time budget in seconds
    *   (2): detail for the name (e.g. to store alternative experiments)
    */

  private val matrixFile = args(0)
  private val timeLimit = args(1).toLong*1000*1000*1000
  private val NAMEALT = args(2)
  private var completed = false
  private var failed = true
  def setFailed(f: Boolean): Unit = failed = f


  private val methodName = {"BB"}
  private val methodDescription = "Branch and Bound using as key: [UP * PositiveFraction + (1 - PositiveFraction) * OBJ]"

  def getName(): String = methodName + NAMEALT
  def getDescription(): String = methodDescription
  def getTimeOfSolutions(): Array[Long] = TIMES.toArray
  def getScoreOfsolutions(): Array[Float] = SCORES.toArray
  def getTimeToComplete(): Long = requiredTime
  def getTimeBest(): Long = TIMES.last
  def getScoreBest(): Float = best.getObjectiveValue()
  def isCompleted(): Boolean = completed
  def getTimeBudget(): Long = timeLimit
  def getFileName(): String = matrixFile
  def isFailed(): Boolean = failed


  // -------------- Reading the matrix --------------

  // Prepare the matrix
  //TODO: should use permSumCol not the modified matrix as the identified rows and columns wouldn't map to the original file
  val matrix = {
    // Read matrix file
    val matrix = Source.fromFile(matrixFile).getLines().toArray.map(_.split("\t").map(_.toFloat))
    val m = matrix.size
    val n = matrix(0).size
    val matrixRevert = Array.tabulate(n)(j => Array.tabulate(m)(i => matrix(i)(j)))
//    val sumCol = Array.tabulate(n)(j => matrixRevert(j).filter(_ > 0)sum)
    val sumCol = Array.tabulate(n)(j => matrixRevert(j).sum)
    val permSumCol = (0 until n).sortBy(j => sumCol(j))
    val matrixRevertSorted = Array.tabulate(n)(j => matrixRevert(permSumCol(j)))
    for (i <- 0 until m; j <- 0 until n) {
      matrix(i)(j) = matrixRevertSorted(j)(i)
    }
    matrix
  }



  private val nRow: Int = matrix.length
  private val nCol: Int = matrix(0).length

  val cumulativeSumPerRowReverted: Array[Array[Float]] = Array.ofDim[Float](nRow, nCol)
  val cumulativePositiveSumPerRowReverted: Array[Array[Float]] = Array.ofDim[Float](nRow, nCol)
  val currentSumPerRow: Array[Float] = new Array[Float](nRow)

  val sumPositiveCol: Array[Float] = new Array[Float](nCol)

  val sparseRow = Array.tabulate(nRow)(i => i)
  var maxRow = nRow

  private var current: Candidate = _
  private var best: Candidate = _

  private var queue: mutable.PriorityQueue[Candidate] = new mutable.PriorityQueue[Candidate]()

  private var branching: Int = 0
  private var pruned: Int = 0
  private var improvements: Int = 0
  private var maxUpB: Float = 0.0f
  private var posToAllRatio: Float = 0.0f
  private var time = System.nanoTime()
  private var requiredTime = 0l
  private val SCORES: ArrayBuffer[Float] = new ArrayBuffer[Float]()
  private val TIMES: ArrayBuffer[Long] = new ArrayBuffer[Long]()

  // Preprocessing ---------------------------------------------------------------------------------------------------------------
  var cntP = 0.0f
  var cntM = 0.0f
  for(c <- 0 until nCol){
    for(r <- 0 until nRow) {
      sumPositiveCol(c) += Math.max(0, matrix(r)(c))
    }
  }
  for(r <- 0 until nRow){
    cumulativeSumPerRowReverted(r)(nCol-1) = matrix(r)(nCol-1)
    cumulativePositiveSumPerRowReverted(r)(nCol-1) = Math.max(0, matrix(r)(nCol-1))
    currentSumPerRow(r) += matrix(r)(nCol-1)
    if(matrix(r)(nCol-1) >= 0) {
      cntP += matrix(r)(nCol-1)
    } else {
      cntM += -matrix(r)(nCol-1)
    }
    for(c <- nCol - 2 to 0 by -1){
      cumulativeSumPerRowReverted(r)(c) = cumulativeSumPerRowReverted(r)(c + 1) + matrix(r)(c)
      cumulativePositiveSumPerRowReverted(r)(c) = cumulativePositiveSumPerRowReverted(r)(c + 1) + Math.max(0, matrix(r)(c))
      currentSumPerRow(r) += matrix(r)(c)
      if(matrix(r)(c) >= 0) {
        cntP += matrix(r)(c)
      } else {
        cntM += -matrix(r)(c)
      }
    }
  }
  posToAllRatio = (cntP/(cntP + cntM))

  // Initialize ---------------------------------------------------------------------------------------------------------------
  println("Time: " + Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + "h, " + Calendar.getInstance().get(Calendar.MINUTE) + "m, " + Calendar.getInstance().get(Calendar.SECOND) + "s")
  time = System.nanoTime()

  //TODO: should start with a defined solution with no selection...
  //Start with full selection of columns
  current = computeBoundAndScoreInit()//computeBoundAndScore(-1, new Array[Int](0))
  //Store it as the next branch and best solution so far
  updateBestSoFar(current)
  store(current)
  compareToBestSoFar(current)

  // Run ---------------------------------------------------------------------------------------------------------------
  while(!isFinished()){
    getNextCandidate()
    branch()
  }
  requiredTime = System.nanoTime()-time
  completed = true
  failed = false
  println(toString())
  storeJson()

  def computeBoundAndScoreInit() = {
    var upperBound: Float = 0.0f
    var objectiveValue: Float = 0.0f

    for(r <- 0 until nRow) {
      upperBound += Math.max(0, cumulativePositiveSumPerRowReverted(r)(0))
      objectiveValue += Math.max(0, cumulativeSumPerRowReverted(r)(0))
    }
    new Candidate(upperBound, objectiveValue, -1, new Array[Int](0))
  }
  def computeBoundAndScore(searchStart: Int, selected: Array[Int]) = {
    var upperBound: Float = 0.0f
    var objectiveValue: Float = 0.0f

    val complemented: Boolean = 2 * selected.length > searchStart+1
    // If there are more elements to add than to substract
    if(complemented) {
      val complement: Array[Int] = new Array[Int](searchStart - selected.length)
      var selectedPos: Int = 0
      var complementPos: Int = 0
      for (c <- 0 until searchStart) {
        if (selectedPos >= selected.length || selected(selectedPos) != c) {
          complement(complementPos) = c
          complementPos += 1
        } else {
          selectedPos += 1
        }
      }

      for (r <- 0 until nRow) {
        currentSumPerRow(r) = cumulativeSumPerRowReverted(r)(0) - cumulativeSumPerRowReverted(r)(searchStart)
        for (c <- complement) {
          currentSumPerRow(r) -= matrix(r)(c)
        }
      }
    } else {
      for (r <- 0 until nRow) {
        currentSumPerRow(r) = 0.0f
        for (c <- selected) {
          currentSumPerRow(r) += matrix(r)(c)
        }
      }
    }
    maxRow = 0
    sumPositiveCol(searchStart) = 0
    if(searchStart + 1 < nCol) {
      for(r <- 0 until nRow) {
        upperBound += Math.max(0, currentSumPerRow(r) + cumulativePositiveSumPerRowReverted(r)(searchStart + 1))
        objectiveValue += Math.max(0, currentSumPerRow(r) + cumulativeSumPerRowReverted(r)(searchStart + 1))
        val a = currentSumPerRow(r) + cumulativePositiveSumPerRowReverted(r)(searchStart)
        if(a > 0){
//          upperBound += a
          sparseRow(maxRow) = r
          maxRow += 1
          sumPositiveCol(searchStart) += Math.max(0, matrix(r)(searchStart))
        }
      }
    } else {
      for(r <- 0 until nRow) {
        upperBound += Math.max(0, currentSumPerRow(r))
        objectiveValue += Math.max(0, currentSumPerRow(r))
        if(currentSumPerRow(r) > 0){
          sparseRow(maxRow) = r
          maxRow += 1
        }
      }
    }
    new Candidate(upperBound, objectiveValue, searchStart, selected)
  }

  def computeBoundAndScoreSwap(searchStart: Int, selected: Array[Int]): Candidate = {
    var upperBound: Float = 0.0f
    var objectiveValue: Float = 0.0f

    if(searchStart > 0) {
      sumPositiveCol(searchStart) = 0
      var r = 0
      while(r < maxRow) {
        currentSumPerRow(sparseRow(r)) += matrix(sparseRow(r))(searchStart - 1)
        if(currentSumPerRow(sparseRow(r)) + cumulativePositiveSumPerRowReverted(sparseRow(r))(searchStart) < 0){
          sparseRow(r) = sparseRow(maxRow-1)
          maxRow -= 1
          r -= 1
        } else {
          sumPositiveCol(searchStart) += Math.max(0, matrix(sparseRow(r))(searchStart))
        }
        r += 1
      }
//      for(r <- 0 until maxRow){
//        currentSumPerRow(sparseRow(r)) += matrix(sparseRow(r))(searchStart - 1)
//      }
//      for (r <- 0 until nRow) {
//        currentSumPerRow(r) += matrix(r)(searchStart - 1)
//      }
    }

    if(searchStart + 1 < nCol) {
      for(r <- 0 until maxRow){
        upperBound += Math.max(0, currentSumPerRow(sparseRow(r)) + cumulativePositiveSumPerRowReverted(sparseRow(r))(searchStart + 1))
        objectiveValue += Math.max(0, currentSumPerRow(sparseRow(r)) + cumulativeSumPerRowReverted(sparseRow(r))(searchStart + 1))
      }
//      var r = 0
//      while(r < maxRow){
////        upperBound += Math.max(0, currentSumPerRow(sparseRow(r)) + cumulativePositiveSumPerRowReverted(sparseRow(r))(searchStart + 1))
//        objectiveValue += Math.max(0, currentSumPerRow(sparseRow(r)) + cumulativeSumPerRowReverted(sparseRow(r))(searchStart + 1))
//        val a = currentSumPerRow(sparseRow(r)) + cumulativePositiveSumPerRowReverted(sparseRow(r))(searchStart + 1)
//        if(a > 0) {
//          upperBound += a
//        } else if (a + math.abs(matrix(sparseRow(r))(searchStart)) < 0){
//          sparseRow(r) = sparseRow(maxRow-1)
//          maxRow -= 1
//          r -= 1
//        }
//        r += 1
//      }
//      for(r <- 0 until nRow) {
//        upperBound += Math.max(0, currentSumPerRow(r) + cumulativePositiveSumPerRowReverted(r)(searchStart + 1))
//        objectiveValue += Math.max(0, currentSumPerRow(r) + cumulativeSumPerRowReverted(r)(searchStart + 1))
//      }
    } else {
      for(r <- 0 until maxRow){
        upperBound += Math.max(0, currentSumPerRow(sparseRow(r)))
        objectiveValue += Math.max(0, currentSumPerRow(sparseRow(r)))
      }
//      var r = 0
//      while(r < maxRow){
//        upperBound += Math.max(0, currentSumPerRow(sparseRow(r)))
//        objectiveValue += Math.max(0, currentSumPerRow(sparseRow(r)))
//        if(currentSumPerRow(sparseRow(r)) < 0){
//          sparseRow(r) = sparseRow(maxRow-1)
//          maxRow -= 1
//          r -= 1
//        }
//        r += 1
//      }
//      for(r <- 0 until nRow) {
//        upperBound += Math.max(0, currentSumPerRow(r))
//        objectiveValue += Math.max(0, currentSumPerRow(r))
//      }
    }
    new Candidate(upperBound, objectiveValue, searchStart, selected)
  }

  def updateBestSoFar(candidate: Candidate): Unit = {
    best = candidate
    TIMES.append(System.nanoTime()-time)
    SCORES.append(best.getObjectiveValue())
    maxUpB = reduceHeapSize()
    println(toString())
  }

  def reduceHeapSize(): Float = {
    var maxUp: Float = 0.0f
    val prevSize: Int = queue.size
    queue = queue.filter(isValidCandidate(_))
    if(!queue.isEmpty){
      maxUp = queue.maxBy(c => c.getUpperBound()).getUpperBound()
    }
    pruned += (prevSize - queue.size)
    Math.max(maxUp, current.getUpperBound())
//    maxUp
  }

  def isValidCandidate(candidate: Candidate): Boolean = {
    candidate.getUpperBound() > best.getObjectiveValue()
  }

  def store(candidate: Candidate): Boolean = {
    if(isValidCandidate(candidate) && (candidate.getUpperBound() > candidate.getObjectiveValue() )){
      queue.enqueue(candidate)
      true
    } else {
      pruned += 1
      false
    }
  }

  def compareToBestSoFar(candidate: Candidate): Unit = {
    if(candidate.getObjectiveValue() > best.getObjectiveValue()){
      improvements += 1
      updateBestSoFar(candidate)
    }
  }

  def isFinished(): Boolean = queue.isEmpty

  def getNextCandidate(): Unit = {
    branching += 1
    current = queue.dequeue()
//    if(branching%50000 == 0){
//      val cTime: Long = (System.nanoTime()-time)/1000000000
//      val h = (cTime/(3600)).toInt
//      val m = ((cTime%3600)/60).toInt
//      val s = (cTime%60).toInt
//      println("\t\t" + best + "\tMax Up: " + maxUpB + "\tGap: " + "%.2f".format(((maxUpB-best.getObjectiveValue())/(best.getObjectiveValue()))*100f) + "%\tTime: " + h + "h, " + m + "m, " + s + "s\n" + "Branched: " + branching + "\tPruned: " + pruned + "\tImprovements: " + improvements + "\tRemaining: " + queue.size)
//    }
  }

  def branch(): Unit = {

    var selected: Array[Int] = current.getSelectedCols()
    current = computeBoundAndScore(current.getSearchPosition() + 1, selected)
    compareToBestSoFar(current)
    var diff = current.getUpperBound() - best.getObjectiveValue()
    if(diff > 0){
      queue.enqueue(current)
    } else if(-diff > sumPositiveCol(current.getSearchPosition())) {
      pruned += 1
      return
    }
    for (c <- current.getSearchPosition() + 1 until nCol - 1) {
      selected = selected ++ Array(c - 1)
      current = computeBoundAndScoreSwap(c, selected)
      compareToBestSoFar(current)
      diff = current.getUpperBound() - best.getObjectiveValue()
      if (diff > 0) {
        queue.enqueue(current)
      } else if (-diff > sumPositiveCol(c)) {
        pruned += 1
        return
      }
    }
    selected = selected ++ Array(nCol - 1)
    current = computeBoundAndScoreSwap(nCol - 1, selected)
    compareToBestSoFar(current)
  }

  override def toString(): String = {
//    val cTime: Long = (System.nanoTime()-time)/1000000000
//    val h = (cTime/(3600)).toInt
//    val m = ((cTime%3600)/60).toInt
//    val s = (cTime%60).toInt
//    best + "\tMax Up: " + maxUpB + "\tGap: " + "%.2f".format(((maxUpB-best.getObjectiveValue())/(best.getObjectiveValue()))*100f) + "%\tTime: " + h + "h, " + m + "m, " + s + "s\n" + "Branched: " + branching + "\tPruned: " + pruned + "\tImprovements: " + improvements
    best + "\tMax Up: " + maxUpB + "\tGap: " + "%.2f".format(((maxUpB-best.getObjectiveValue())/(best.getObjectiveValue()))*100f) + "\tTime: " + (System.nanoTime()-time)
  }



  class Candidate(private var upperBound: Float, private var objectiveValue: Float, private var searchPosition: Int, private var selectedCols: Array[Int]) {

    private val key: Float = {
        upperBound * posToAllRatio + (1 - posToAllRatio) * objectiveValue
    }
    def getUpperBound(): Float = upperBound
    def getObjectiveValue(): Float = objectiveValue
    def getSearchPosition(): Int = searchPosition
    def getSelectedCols(): Array[Int] = selectedCols

    override def toString(): String = objectiveValue + "\t[" + upperBound + "]"
  }

  object Candidate {
    implicit def orderingByKey[A <: Candidate]: Ordering[A] = Ordering.by(c => c.key)
  }
}
