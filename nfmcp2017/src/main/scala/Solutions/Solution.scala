package Solutions

import utils.{FileReporter, JsonBuilder}

/**
  * Created by vbranders on 21/02/17.
  *
  */
trait Solution {

  def getName(): String
  def getDescription(): String
  def getTimeOfSolutions(): Array[Long]
  def getScoreOfsolutions(): Array[Float]
  def getTimeToComplete(): Long
  def getTimeBest(): Long
  def getScoreBest(): Float
  def isCompleted(): Boolean
  def isFailed(): Boolean
  def getTimeBudget(): Long
  def getFileName(): String
  def setFailed(f: Boolean): Unit

  def storeJson(): Unit = {
    val res: JsonBuilder = new JsonBuilder()
    res.add("name", getName())
    res.add("description", getDescription())
    res.add("times", getTimeOfSolutions())
    res.add("scores", getScoreOfsolutions())
    res.add("bestScore", getScoreBest)
    res.add("timeToBest", getTimeBest)
    res.add("completed", isCompleted().toString)
    res.add("timeBudget", getTimeBudget())
    res.add("timeToStop", getTimeToComplete())
    res.add("failed", isFailed().toString)
    new FileReporter(res.toString(), getFileName().substring(0, getFileName().lastIndexOf('.')) +  "." + getName() + ".JSON")
  }
}
