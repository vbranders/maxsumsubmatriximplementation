package Solutions.MIP




import Solutions.Solution
import gurobi._

import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
  * @author Pierre Schaus
  */
object MaxSumSubMatrixMIP extends App with Solution {

  /**
    * Arg:
    *   (0): file path to matrix
    *   (1): time budget in seconds
    *   (2): detail for the name (e.g. to store alternative experiments)
    *   (3): true to use the model with one decision var per cell entry, false to use sum of lines
    */

  private val matrixFile = args(0)
  private val timeLimit = args(1).toLong*1000*1000*1000
  private val NAMEALT = args(2)
  private val useModelWithSums = args(3).toBoolean
  private var completed = false
  private var failed = true


  private val methodName = {if(useModelWithSums) "MIP" else "MIP_full"}
  private val methodDescription = {if(useModelWithSums) "MIP using gurobi" else "MIP using gurobi with the model with one var per cell"}

  def getName(): String = methodName + NAMEALT
  def getDescription(): String = methodDescription
  def getTimeOfSolutions(): Array[Long] = if(TIMES.length == 0) Array(0l) else TIMES.toArray
  def getScoreOfsolutions(): Array[Float] =  if(SCORES.length == 0) Array(0f) else SCORES.toArray
  def getTimeToComplete(): Long = requiredTime
  def getTimeBest(): Long =  if(TIMES.length == 0) timeLimit else TIMES.last
  def getScoreBest(): Float = bestScore.toFloat
  def isCompleted(): Boolean = completed
  def getTimeBudget(): Long = timeLimit
  def getFileName(): String = matrixFile
  def isFailed(): Boolean = failed
  def setFailed(f: Boolean): Unit = failed = f


  // -------------- Reading the matrix --------------
  var bestScore = Double.MinValue

  val matrix = Source.fromFile(args(0)).getLines().toArray.map(_.split("\t").map(_.toDouble))


  val n = matrix.size
  val m = matrix(0).size


  val env: GRBEnv = new GRBEnv(matrixFile+".mip.log")
  val model: GRBModel = new GRBModel(env)

  val objective = new GRBLinExpr()

  val xColumns: Array[GRBVar] = Array.tabulate(m)(j => model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "xc"+j))

  if(useModelWithSums){
    val si: Array[GRBLinExpr] = Array.ofDim(n)
    for(i <- 0 until n){
      val term = new GRBLinExpr()
      for(j <- 0 until m) {
        term.addTerm(matrix(i)(j), xColumns(j))
      }
      si(i) = term
    }
    val b0: Array[GRBVar] = Array.tabulate(n)(i => model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "b0"+i))
    val posi: Array[GRBVar] = Array.ofDim(n)
    for(i <- 0 until n){
      var lb = 0d
      var ub = 0d
      for(j <- 0 until m) {
        lb += math.min(0, matrix(i)(j))
        ub += math.max(0, matrix(i)(j))
      }
      posi(i) = model.addVar(lb, ub, 0.0, GRB.CONTINUOUS, "posi"+i)
      val term = new GRBLinExpr()
      term.addConstant(ub) //1*ub
      term.addTerm(-ub, b0(i)) //-1 * ub
      model.addConstr(posi(i),GRB.LESS_EQUAL,term,"four")
      val term2 = new GRBLinExpr()
      term2.addTerm(-lb, b0(i)) //1*-lb
      term2.add(si(i))
      model.addConstr(posi(i),GRB.LESS_EQUAL,term2,"five")
    }

    for (i <- 0 until n) {
      objective.addTerm(1.0,posi(i))
    }
  } else {
      val xRows: Array[GRBVar] = Array.tabulate(n)(i => model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "xr"+i))
      val x: Array[Array[GRBVar]] = Array.tabulate(n,m){case(i,j) => model.addVar(0.0, 1.0, 0.0, GRB.BINARY, s"x($i,$j)")}

      for (i <- 0 until n; j <- 0 until m) {
        model.addConstr(x(i)(j),GRB.LESS_EQUAL,xRows(i),"one");
        model.addConstr(x(i)(j),GRB.LESS_EQUAL,xColumns(j),"two");

        val term = new GRBLinExpr();
        term.addTerm(1.0,xRows(i))
        term.addTerm(1.0,xColumns(j))
        term.addConstant(-1)
        model.addConstr(x(i)(j),GRB.GREATER_EQUAL,term,"three")
      }

      for (i <- 0 until n; j <- 0 until m) {
        objective.addTerm(matrix(i)(j),x(i)(j))
      }
  }

  private val time = System.nanoTime()
  private var requiredTime = 0l
  private val SCORES: ArrayBuffer[Float] = new ArrayBuffer[Float]()
  private val TIMES: ArrayBuffer[Long] = new ArrayBuffer[Long]()

  model.setObjective(objective, GRB.MAXIMIZE)

  val callback = new StoreProgression


  model.setCallback(callback)

  model.set(GRB.IntParam.Presolve, 0)
  //model.set(GRB.IntParam.Cuts, 0)
  model.set(GRB.IntParam.Threads, 1)

  model.optimize

  bestScore = model.get(GRB.DoubleAttr.ObjVal).toFloat
  System.out.println("Obj: " + model.get(GRB.DoubleAttr.ObjVal))

  // Dispose of model and environment
  model.dispose
  env.dispose

  println("Terminated")
  requiredTime = System.nanoTime()-time
  println(requiredTime)
  println(bestScore)
  completed = true
  failed = false
  storeJson()



  class StoreProgression extends GRBCallback {

    def callback() = {
      if(where == GRB.CB_MIPSOL){
        val obj: Double = getDoubleInfo(GRB.CB_MIPSOL_OBJ)
        TIMES.append(System.nanoTime()-time)
        SCORES.append(obj.toFloat)
        bestScore = obj.toFloat

        val cTime: Long = (System.nanoTime()-time)/1000000000
        val h = (cTime/(3600)).toInt
        val m = ((cTime%3600)/60).toInt
        val s = (cTime%60).toInt
        println("Best solution: " + obj + "\tTime: " + h + "h, " + m + "m, " + s + "s")
      }
    }

  }

}
