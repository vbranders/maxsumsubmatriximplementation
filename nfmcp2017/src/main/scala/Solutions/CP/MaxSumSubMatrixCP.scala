package Solutions.CP

import Solutions.Solution
import oscar.cp._
import oscar.cp.core.CPSolver
import oscar.cp.core.variables.CPBoolVar

import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
  * @author Pierre Schaus
  */
object MaxSumSubMatrixCP extends App with Solution {

  /**
    * Arg:
    *   (0): file path to matrix
    *   (1): time budget in seconds
    *   (2): detail for the name (e.g. to store alternative experiments)
    *   (3): true to use LNS, false otherwise
    */

  private val matrixFile = args(0)
  private val timeLimit = args(1).toLong*1000*1000*1000
  private val NAMEALT = args(2)
  private var completed = false
  private val useLNS = args(3).toBoolean
  private var failed = true

  private val methodName = {if(useLNS) "CP_LNS" else "CP_Global"}
  private val methodDescription = {if(useLNS) "CP using LNS" else "CP using global constraint"}

  def getName(): String = methodName + NAMEALT
  def getDescription(): String = methodDescription
  def getTimeOfSolutions(): Array[Long] = TIMES.toArray
  def getScoreOfsolutions(): Array[Float] = SCORES.toArray
  def getTimeToComplete(): Long = requiredTime
  def getTimeBest(): Long = TIMES.last
  def getScoreBest(): Float = bestScore.toFloat
  def isCompleted(): Boolean = completed
  def getTimeBudget(): Long = timeLimit
  def getFileName(): String = matrixFile
  def isFailed(): Boolean = failed
  def setFailed(f: Boolean): Unit = failed = f


  // -------------- Reading the matrix --------------

  var bestScore = Double.MinValue
  val matrix = Source.fromFile(matrixFile).getLines().toArray.map(_.split("\t").map(_.toDouble))


  val n = matrix.size
  val m = matrix(0).size


  println("root UB:" + matrix.map(_.filter(_ > 0).sum).sum)

  // -------------- CP Model --------------
  implicit val cp = CPSolver()

  val x = Array.fill[CPBoolVar](m)(CPBoolVar())

  val xBest = Array.ofDim[Int](m)


  val xsorted = {
    val matrixRevert = Array.tabulate(m)(j => Array.tabulate(n)(i => matrix(i)(j)))
    val sumCol = Array.tabulate(m)(j => matrixRevert(j).filter(_ > 0).sum)
    val permSumCol = (0 until m).sortBy(j => -sumCol(j))
    Array.tabulate(m)(i => x(permSumCol(i)))
  }

  private val time = System.nanoTime()
  private var requiredTime = 0l
  private val SCORES: ArrayBuffer[Float] = new ArrayBuffer[Float]()
  private val TIMES: ArrayBuffer[Long] = new ArrayBuffer[Long]()


  val maxSumConstraint = new MaxSumSubMatrixConstraint(x, matrix)

  cp.add(maxSumConstraint)

  maxSumConstraint.onSolution = () => {
    TIMES.append(System.nanoTime() - time)
    bestScore = maxSumConstraint.bestBound
    SCORES.append(bestScore.toFloat)
    println("new best bound:" + bestScore + "\tTime: " + (System.nanoTime() - time))
    // record best solution
    for (i <- 0 until x.size) {
      if (x(i).isTrue) xBest(i) = 1
      else xBest(i) = 0
    }
    // println("new best bound:" + maxSumConstraint.bestBound)
//    val cTime: Long = (System.nanoTime()-time)/1000000000
//    val h = (cTime/(3600)).toInt
//    val m = ((cTime%3600)/60).toInt
//    val s = (cTime%60).toInt
//    println("new best bound:" + bestScore + "\tTime: " + h + "h, " + m + "m, " + s + "s")
  }


  cp.search {
    binaryStaticIdx(xsorted, i => xsorted(i).max)
  }

  // Find first solution to initialize LNS with limited number of backtracks

  if (!useLNS) {
    val stat = cp.start()
    println(stat)
  } else {
    // Large Neighorhood Search
    val stat = cp.start(failureLimit = 5000, maxDiscrepancy = 2)
    val rand = new scala.util.Random(0)
    var i = 0
    while(true){
      i += 1
//    for (i <- 0 until 100) {
//      println("restart:" + i)
      val stat = cp.startSubjectTo(failureLimit = 5000, maxDiscrepancy = 2) {
        cp.add((0 until m).filter(i => rand.nextInt(100) < 80).map(i => x(i) === xBest(i)))
      }
//      println("completed:" + stat.completed)
    }
  }
  requiredTime = System.nanoTime()-time
  println(requiredTime)
  completed = true
  failed = false
  storeJson()

}
