package Solutions.CP

import Solutions.Solution
import oscar.cp._
import oscar.cp.core.CPSolver
import oscar.cp.core.variables.CPBoolVar

import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
  * @author Pierre Schaus
  */
object MaxSumSubMatrixCPLeVan extends App with Solution {

  /**
    * Arg:
    *   (0): file path to matrix
    *   (1): time budget in seconds
    *   (2): detail for the name (e.g. to store alternative experiments)
    *   (3): true to use LNS, false otherwise
    *   (4): true to use matrix as is, false to convert float to int
    *   (5): true to use the default model of Le Van
    */

  private val matrixFile = args(0)
  private val timeLimit = args(1).toLong*1000*1000*1000
  private val NAMEALT = args(2)
  private var completed = false
  private val useLNS = args(3).toBoolean
  private val useMatrixAsInt = args(4).toBoolean
  private val useDefault = if(args.length > 5) args(5).toBoolean else false
  private var failed = true


  private val methodName = if(NAMEALT.length == 0){"LeVan"+{if(!useDefault) "_Imp" else ""}+{if(useLNS) "_LNS" else ""}} else {""}
  private val methodDescription = "LeVan"+{if(!useDefault) " improved" else ""}+{if(useLNS) " using LNS" else ""}+"."

  def getName(): String = methodName + NAMEALT
  def getDescription(): String = methodDescription
  def getTimeOfSolutions(): Array[Long] =  if(TIMES.length == 0) Array(0l) else TIMES.toArray
  def getScoreOfsolutions(): Array[Float] =  if(SCORES.length == 0) Array(0f) else SCORES.toArray
  def getTimeToComplete(): Long = requiredTime
  def getTimeBest(): Long =  if(TIMES.length == 0) timeLimit else TIMES.last
  def getScoreBest(): Float = bestScore
  def isCompleted(): Boolean = completed
  def getTimeBudget(): Long = timeLimit
  def getFileName(): String = matrixFile
  def isFailed(): Boolean = failed
  def setFailed(f: Boolean): Unit = failed = f


  // -------------- Reading the matrix --------------

  var bestScore = Float.MinValue
  var rate = 0.0f
  val mat = Source.fromFile(matrixFile).getLines().toArray.map(_.split("\t").map(_.toFloat))
  val matrix = {
    if(useMatrixAsInt) {
      Source.fromFile(matrixFile).getLines().toArray.map(_.split("\t").map(x => math.ceil(x.toDouble).toInt))
    } else {
      val coef = Int.MaxValue / (mat.length * mat(0).length)
      val ma = mat.map(_.max).max
      val mi = mat.map(_.min).min
      rate = if (-mi > ma) {
        -coef / mi
      } else {
        coef / ma
      }
      Array.tabulate(mat.length)(i => Array.tabulate(mat(0).length)(j => (mat(i)(j)*rate).toInt))
    }
  }


  val n = matrix.size
  val m = matrix(0).size


  println("root UB:" + matrix.map(_.filter(_ > 0).sum).sum)

  // -------------- CP Model --------------
  implicit val cp = CPSolver()

  val x = Array.fill[CPBoolVar](m)(CPBoolVar())
  val y = Array.fill[CPBoolVar](n)(CPBoolVar())

  val xBest = Array.ofDim[Int](m)


  val xsorted = {
    val matrixRevert = Array.tabulate(m)(j => Array.tabulate(n)(i => matrix(i)(j)))
    val sumCol = Array.tabulate(m)(j => matrixRevert(j).filter(_ > 0).sum)
//    val sumCol = Array.tabulate(m)(j => matrixRevert(j).sum)
    val permSumCol = (0 until m).sortBy(j => -sumCol(j))
    Array.tabulate(m)(i => x(permSumCol(i)))
  }

  private val time = System.nanoTime()
  private var requiredTime = 0l
  private val SCORES: ArrayBuffer[Float] = new ArrayBuffer[Float]()
  private val TIMES: ArrayBuffer[Long] = new ArrayBuffer[Long]()

  val objective = if(useDefault) {
    println("Default !!!!!!!!!!!!!!!")
  /*
  // model of Le Van et al (less efficient)

  for (i <- 0 until n) {
    // y(i) is taken only iff the sum is positive
    add((sum(0 until m)(j => x(j) * matrix(i)(j)) ?>= 0) === y(i))
  }
  val objective = sum(for (i <- 0 until n; j <- 0 until m) yield (x(j) && y(i)) * matrix(i)(j))
  */

    for (i <- 0 until n) {
      // y(i) is taken only iff the sum is positive
      add((sum(0 until m)(j => x(j) * matrix(i)(j)) ?>= 0) === y(i))
    }
    for (j <- 0 until m) {
      // x(j) is taken only iff the sum is positive
      add((sum(0 until n)(i => y(i) * matrix(i)(j)) ?>= 0) === x(j))
    }
    sum(for (i <- 0 until n; j <- 0 until m) yield (x(j) && y(i)) * matrix(i)(j))

  } else {
    println("not default *************")
    val sumLine = Array.tabulate(n)(i => sum(0 until m)(j => x(j) * matrix(i)(j)))
    sum(Array.tabulate(n)(i => (sumLine(i) ?>= 0) * sumLine(i)))
  }

  // better  model (more pruning)
//  val sumLine = Array.tabulate(n)(i => sum(0 until m)(j => x(j) * matrix(i)(j)))
//  val objective = sum(Array.tabulate(n)(i => (sumLine(i) ?>= 0) * sumLine(i)))

  maximize(objective)

  cp.search {
    binaryStaticIdx(xsorted, i => xsorted(i).max)
  }

  onSolution {
    TIMES.append(System.nanoTime()-time)

    // record best solution
    for (i <- 0 until x.size) {
      if (x(i).isTrue) xBest(i) = 1
      else xBest(i) = 0
    }

    bestScore = {
      if(useMatrixAsInt){
        objective.value
      } else {
        var s = 0.0f
        for (i <- 0 until mat.size) {
          var sR = 0.0f
          for (j <- 0 until mat(0).size) {
            if (xBest(j) == 1) {
              sR += mat(i)(j)
            }
          }
          if (sR > 0.0f) {
            s += sR
          }
        }
        s
      }
    }
    SCORES.append(bestScore)
    println("new best bound:" + bestScore)
  }

  // Find first solution to initialize LNS with limited number of backtracks

  val rand = new scala.util.Random(0)
  if (!useLNS) {
    val stat = cp.start()
    println(stat)
  } else {
    // Large Neighorhood Search
    val stat = cp.start(failureLimit = 100)
    var i = 0
    while(true){
      i += 1
//    for (i <- 0 until 100) {
//      println("restart:" + i)
      val stat = cp.startSubjectTo(failureLimit = 100) {
        cp.add((0 until m).filter(i => rand.nextInt(100) < 50).map(i => x(i) === xBest(i)))
      }
//      println("completed:" + stat.completed)
    }
  }
  requiredTime = System.nanoTime()-time
  println(requiredTime)
  completed = true
  failed = false
  storeJson()

}
