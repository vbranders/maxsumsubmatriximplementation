
package Solutions.CP;

import oscar.algo.reversible._
import scala.language.implicitConversions

/**
  * @author Pierre Schaus  pschaus@gmail.com
  * @author Renaud Hartert ren.hartert@gmail.com
  */
class ReversibleDouble(context: ReversibleContext, initValue: Double, initSize: Int) extends TrailEntry {

  def this(context: ReversibleContext, initValue: Double) = this(context, initValue, 32)

  // Inner trailing queue
  private[this] var trail = new Array[Double](initSize)
  private[this] var trailSize = 0

  // Current value
  private[this] var pointer: Double = initValue

  // Id of the last context
  private[this] var lastMagic: Long = -1L

  @inline private def trail(): Unit = {
    val contextMagic = context.magic
    if (lastMagic != contextMagic) {
      lastMagic = contextMagic
      if (trailSize == trail.length) growTrail()
      trail(trailSize) = pointer
      trailSize += 1
      context.trail(this)
    }
  }

  final override def restore(): Unit = {
    trailSize -= 1
    pointer = trail(trailSize)
  }

  /** Increments the reversible integer by one */
  @inline final def incr(): Double = {
    trail()
    pointer += 1
    pointer
  }

  /** Decrements the reversible integer by one */
  @inline final def decr(): Double = {
    trail()
    pointer -= 1
    pointer
  }

  /** Increments the reversible integer by i */
  @inline final def +=(i: Double): Double = {
    trail()
    pointer += i
    pointer
  }

  /** Decrements the reversible integer by i */
  @inline final def -=(i: Double): Double = {
    trail()
    pointer -= i
    pointer
  }

  @inline final def setValue(value: Double): Unit = {
    if (value != pointer) {
      trail()
      pointer = value
    }
  }

  /** @param value to assign */
  @inline final def value_= (value: Double): Unit = setValue(value)

  /** @param value to assign */
  final def := (value: Double): Unit = setValue(value)

  /** @return current value */
  @inline final def value: Double = pointer

  /** @return the current pointer */
  @inline final def getValue(): Double = pointer

  override def toString(): String = pointer.toString

  @inline private def growTrail(): Unit = {
    val newTrail = new Array[Double](trailSize * 2)
    System.arraycopy(trail, 0, newTrail, 0, trailSize)
    trail = newTrail
  }
}

object ReversibleDouble {
  def apply(value: Double)(implicit context: ReversibleContext) = new ReversibleDouble(context, value)
  implicit def reversibleDoubleToValue(reversible: ReversibleDouble): Double = reversible.value
}
