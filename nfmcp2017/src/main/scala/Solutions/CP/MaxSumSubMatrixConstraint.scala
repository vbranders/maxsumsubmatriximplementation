package Solutions.CP

import oscar.algo.Inconsistency
import oscar.algo.reversible.ReversibleInt
import oscar.cp._
import oscar.cp.core.CPPropagStrength
import oscar.cp.core.variables.{CPBoolVar, CPVar}

/**
  * Global Constraint also doing the branch and bound internally as at every level of decision
  * we have a feasible decision.
  */
class MaxSumSubMatrixConstraint(x: Array[CPBoolVar], matrix: Array[Array[Double]]) extends Constraint(x(0).store) {

  var onSolution: () => Unit = () => {}


  val n = matrix.size
  val m = matrix(0).size

  val partialSum = Array.tabulate(n)(i => new ReversibleDouble(s, 0.0))

  // Reversible sparse-set trick to maintain unbound variables
  val unBound = Array.tabulate(m)(i => i)
  val nUnBound = new ReversibleInt(s, m)

  // Reversible sparse-set trick to maintain possibly interesting lines
  val candidate = Array.tabulate(n)(i => i)
  val nCandidate = new ReversibleInt(s, n)



  var bestBound = Double.MinValue


  def setup(l: CPPropagStrength): Unit = {
    // propagate method is called when any of the variables is bound
    for (i <- 0 until m) {
      x(i).callPropagateWhenBind(this);
    }
    propagate()
  }

  def removeBoundVariablesAndUpdatePartialSum(): Unit = {
    var size = nUnBound.value
    var j = size
    while (j > 0) {
      j -= 1
      if (x(unBound(j)).isBound) {
        size -= 1
        val tmp = unBound(j)
        if (x(tmp).isTrue) select(tmp)
        unBound(j) = unBound(size)
        unBound(size) = tmp
      }
    }
    nUnBound.value = size
  }

  def select(c: Int): Unit = {
    var i = nCandidate.value
    while (i > 0) {
      i -= 1
      partialSum(candidate(i)) += matrix(candidate(i))(c)
    }
  }

  def computeObjective(): Double = {
    var objective = 0.0
    var i = nCandidate.value
    while (i > 0) {
      i -= 1
      var pSum = partialSum(candidate(i)).value
      if (pSum > 0) {
        objective += pSum
      }
    }
    return objective
  }

  def computeObjectiveUB(): Double = {
    var objectiveUB = 0.0
    var nCand = nCandidate.value
    var i = nCand
    while (i > 0) {
      i -= 1
      var costLineUB = partialSum(candidate(i)).value
      // optimistic selection of positive values only for the UB
      var j = nUnBound.value
      while (j > 0) {
        j -= 1
        if (matrix(candidate(i))(unBound(j)) > 0) {
          costLineUB += matrix(candidate(i))(unBound(j))
        }
      }
      if (costLineUB > 0) {
        objectiveUB += costLineUB
      } else {
        // remove this line as it can never become positive under this node
        nCand -= 1
        val tmp = candidate(i)
        candidate(i) = candidate(nCand)
        candidate(nCand) = tmp
      }
    }
    nCandidate.value = nCand
    return objectiveUB
  }

  var objectiveUBNotSelect = Array.fill[Double](m)(0.0)
  var objectiveUBForceSelect = Array.fill[Double](m)(0)


  def prune(): Unit = {

    var objectiveUB = 0.0
    val nCols = nUnBound.value
    var j: Int = nCols
    while (j > 0) {
      j -= 1
      objectiveUBNotSelect(unBound(j)) = 0
      objectiveUBForceSelect(unBound(j)) = 0
    }

    var nCand = nCandidate.value
    var i = nCand
    while (i > 0) {
      i -= 1
      var costLineUB = partialSum(candidate(i)).value
      // optimistic selection of positive values only for the UB
      j = nCols
      while (j > 0) {
        j -= 1
        if (matrix(candidate(i))(unBound(j)) > 0) {
          costLineUB += matrix(candidate(i))(unBound(j))
        }
      }
      if (costLineUB > 0) {
        objectiveUB += costLineUB

        // objectiveUBNotSelect(j) = upper bound if column j would not be selected
        j = nCols
        while (j > 0) {
          j -= 1
          if (matrix(candidate(i))(unBound(j)) > 0) {
            if (costLineUB - matrix(candidate(i))(unBound(j)) > 0) {
              objectiveUBNotSelect(unBound(j)) += costLineUB - matrix(candidate(i))(unBound(j))
            }
            objectiveUBForceSelect(unBound(j)) += costLineUB
          } else {
            objectiveUBNotSelect(unBound(j)) += costLineUB
            if (costLineUB + matrix(candidate(i))(unBound(j)) > 0) {
              objectiveUBForceSelect(unBound(j)) += costLineUB + matrix(candidate(i))(unBound(j))
            }
          }
        }

      } else {
        // remove this line as it can never become positive under this node
        nCand -= 1
        val tmp = candidate(i)
        candidate(i) = candidate(nCand)
        candidate(nCand) = tmp
      }

    }
    // Pruning of the branch and bound
    if (objectiveUB <= bestBound) {
      throw Inconsistency
    }
    j = nCols
    while (j > 0) {
      j -= 1
      if (objectiveUBNotSelect(unBound(j)) <= bestBound) {
        x(unBound(j)).assignTrue()
      }
      if (objectiveUBForceSelect(unBound(j)) <= bestBound) {
        x(unBound(j)).assignFalse()
      }
    }
    nCandidate.value = nCand
  }

  override def propagate(): Unit = {
    removeBoundVariablesAndUpdatePartialSum()
    val objective = computeObjective()
    if (objective > bestBound) {
      bestBound = objective
      onSolution()
//      println("new best bound:" + objective)
    }
    prune()
  }

  override def associatedVars(): Iterable[CPVar] = x


}
