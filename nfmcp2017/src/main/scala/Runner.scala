import Solutions.BranchAndBound.MaxSumSubMatrixBranchAndBound
import Solutions.CP.{MaxSumSubMatrixCP, MaxSumSubMatrixCPLeVan}
import Solutions.MIP.MaxSumSubMatrixMIP

/**
  * Created by vbranders on 6/03/17.
  *
  */
object Runner extends App {


  /**
    * Args:
    *   (0): name of experiment to do
    *   (1): instance to run
    *   (2): time budget in seconds
    *   (+): remaining arguments
    *
    *   For CP global with LNS:
    *     CP data/paper/Validation/test/0_matrix_int.tsv 10 "_6" true
    *   For BB:
    *     BB data/paper/Validation/test/9_matrix.tsv 20 ""
    */

  println("------------------------------------------------------------------------------------------------------------------------")
  val approach = args(0)
  println("Approach:\t" + approach)
  val path = args(1)
  println("Reading file:\t" + path)
  val timeLimit = args(2).toLong
  println("Time limit:\t" + (timeLimit))
  println("------------------------------------------------------------------------------------------------------------------------")

  val appToRun = approach match {
    case "BB" => MaxSumSubMatrixBranchAndBound
    case "CP" => MaxSumSubMatrixCP
    case "LeVan" => MaxSumSubMatrixCPLeVan
    case "MIP" => MaxSumSubMatrixMIP
    case _ => {println("Error!\nNot found: " + approach);System.exit(0);MaxSumSubMatrixBranchAndBound}
  }
  var thread = new Thread {
    override def run(): Unit = {
      try {
        appToRun.main(args.drop(1))
      } catch {
          case e: Exception => {appToRun.setFailed(true); appToRun.storeJson()}
      }
    }
  }
  thread.start()
  thread.join(timeLimit * 1000)
  val completed = !thread.isAlive
  appToRun.setFailed(false)
  appToRun.storeJson()
  println("\n" + {if(completed) "Completed !" else "Interrupted !"})
  thread.interrupt()
  thread = null
  println("-----------------------------------------------")
  System.exit(0)

}
