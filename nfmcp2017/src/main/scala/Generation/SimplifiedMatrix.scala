package Generation

import java.util

import utils.{FileReporter, JsonBuilder}

import scala.util.Random

/**
  * Created by vbranders on 8/03/17.
  *
  */
object SimplifiedMatrix extends App {

  /**
    * Args:
    *   0:  path to folder
    *   1:  starting seed
    *   2:  ending seed
    *   3:  probability for each column (and row) to be a solution (with high distribution)
    *   4:  number of rows
    *   5:  number of columns
    *   6:  mean of the low distribution
    *   7:  mean of the high distribution
    *   8:  sigma of the low and high distributions
    *   9:  probability for each row to be a solution (with high distribution)
    *
    */

  // Parameters for data generation
  val path: String = args(0)
  val seedMin: Int = defaultOrArg("0", 1).toInt
  val seedMax: Int = defaultOrArg("1", 2).toInt
  val pSol: Float = defaultOrArg("0.2", 3).toFloat
  val sizeMatrix: Array[Int] = Array(defaultOrArg("10000", 4).toInt, defaultOrArg("100", 5).toInt)
  val lowMean: Float = defaultOrArg("-3", 6).toFloat
  val highMean: Float = defaultOrArg("0", 7).toFloat
  val sigma: Float = defaultOrArg("1", 8).toFloat
  val pSolRow: Float = defaultOrArg(pSol.toString, 9).toFloat



  var rand: Random = new Random(seedMin)

  // Data generation
  for(seed <- seedMin until seedMax) {
    rand = new Random(seed)
    //Background noise
    val matrix: Array[Array[Float]] = Array.tabulate(sizeMatrix(0), sizeMatrix(1))((i, j) => sampleGaussian(lowMean, sigma))

//    var a = Array.tabulate[Int](sizeMatrix(0))(i => i)
//    a = rand.shuffle(a.toList).toArray
//    var b = Array.tabulate[Int](sizeMatrix(1))(i => i)
//    b = rand.shuffle(b.toList).toArray
//    val trueRows: Array[Int] = a.slice(0, (sizeMatrix(0)*Math.sqrt(pSol)).toInt)
//    val trueCols: Array[Int] = b.slice(0, (sizeMatrix(1)*Math.sqrt(pSol)).toInt)

    //SolutionFinder
    val trueRows: Array[Int] = Array.tabulate[Int](sizeMatrix(0))(i => i).filter(i => rand.nextFloat() < pSolRow)
    val trueCols: Array[Int] = Array.tabulate[Int](sizeMatrix(1))(i => i).filter(i => rand.nextFloat() < pSol)
    for(r <- trueRows; c <- trueCols){
      matrix(r)(c) = sampleGaussian(highMean, sigma)
    }
    val coverage: Float = (trueCols.size * trueRows.size).toFloat/(sizeMatrix(0) * sizeMatrix(1)).toFloat
    val effectiveCoverage: Float = (matrix.map(_.filter(_ >= 0).size).sum).toFloat/(sizeMatrix(0) * sizeMatrix(1)).toFloat

    // Store parameters
    new FileReporter(getParameters(seed, coverage, effectiveCoverage, trueRows, trueCols), path + seed + "_parameters.JSON")
    // Store matrix
    new FileReporter(matrix.map(_.mkString("\t")).mkString("\n"), path + seed + "_matrix.tsv")
    // Store int matrix
//    new FileReporter(asInt(matrix).map(_.mkString("\t")).mkString("\n"), path + seed + "_matrix_int.tsv")
  }


  def sampleUniform(min: Float, max: Float): Float = min + (rand.nextFloat() * (max - min))
  def sampleGaussian(mean: Float, sigma: Float): Float = mean + (rand.nextGaussian().toFloat * sigma)

  def asInt(matrix: Array[Array[Float]]): Array[Array[Int]] = {
    val UpperBound = matrix.map(_.filter(_ > 0).sum).sum
//    val LowerBound = matrix.map(_.sum).sum
    val LowerBound = 0
    val maxMultiplication = Int.MaxValue/(Math.max(-LowerBound, UpperBound))
    println(maxMultiplication)
    Array.tabulate(matrix.size, matrix(0).size)((i, j) => (matrix(i)(j)*maxMultiplication).toInt)
  }
  def getParameters(seed: Int, coverage: Float, effectiveCoverage: Float, trueRows: Array[Int], trueCols: Array[Int]): String = {
    val json: JsonBuilder = new JsonBuilder()
    json.add("seed", seed)
    json.add("pSol", pSol)
    if(pSol != pSolRow){
      json.add("pSolRow", pSolRow)
    }
    json.add("nRows", sizeMatrix(0))
    json.add("nCols", sizeMatrix(1))
    json.add("coverage", coverage)
    json.add("effectiveCoverage", effectiveCoverage)
    json.add("trueRows", trueRows)
    json.add("trueCols", trueCols)
    json.add("lowMean", lowMean)
    json.add("highMean", highMean)
    json.add("sigma", sigma)
    json.toString()
  }
  def defaultOrArg(default: String, arg: Int): String = if(args.size > arg && args(arg).size > 0) args(arg) else default
}
