package utils

/**
  * Created by vbranders on 22/11/17.
  *
  */
class ArgsParser {

  def usageString(usage: Map[String, Array[String]]): String = {
    var s = "Args: "
    var descr = "\n"
    for(i <- usage.keys){
      s += "[" + i + " <" + usage.get(i).get(0) + ">] "
      descr += "\n\t" + i + " <" + usage.get(i).get(0) + ">\n\t\t" + usage.get(i).get(2)
    }
    s + descr
  }

  def parse(usage: Map[String, Array[String]], args: Array[String]): Map[String, String] = {
    if(args.length == 0){
      println(usageString(usage))
    }
    var r: Map[String, String] = Map()
    for(i <- 0 until args.length-1){ //Add the element from args to the dico
      if(usage.keySet.contains(args(i))){
        r += (args(i) -> args(i+1))
//        println(usage.get(args(i)).get(2) + ": " + args(i+1))
      }
    }
    for(k <- usage.keys){ //Include values from undefined args
      r += (k -> r.getOrElse(k, usage.get(k).get(1)))
    }
    r
  }

}
