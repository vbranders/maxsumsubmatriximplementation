package utils

import scala.collection.mutable.ArrayBuffer

/**
  * Created by vbranders on 17/04/17.
  *
  * Constructor of JSON files.
  *
  */
class JSONBuilder {

  var lineSeparator = sys.props("line.separator")
  var listSeparator = Array("[", "]", ", ")
  var textSeparator = "\""
  var tabulation = "\t"
  var valueSeparator = ": "
  var contentSeparator = "," + lineSeparator
  var content: ArrayBuffer[String] = new ArrayBuffer[String]()

  var indent = 1

  def insert(key: String, value: String): Unit = {
    content += (textSeparator + key + textSeparator + valueSeparator + value)
  }
  def add(key: String, value: String): Unit = insert(key, textSeparator + value + textSeparator)
  def add(key: String, value: Int): Unit = insert(key, value.toString)
  def add(key: String, value: Double): Unit = insert(key, value.toString)
  def add(key: String, table: Array[Int]): Unit = insert(key, listSeparator(0) + table.mkString(listSeparator(2)) + listSeparator(1))
  def add(key: String, table: Array[Float]): Unit = insert(key, listSeparator(0) + table.mkString(listSeparator(2)) + listSeparator(1))
  def add(key: String, table: Array[Double]): Unit = insert(key, listSeparator(0) + table.mkString(listSeparator(2)) + listSeparator(1))
  def add(key: String, table: Array[Long]): Unit = insert(key, listSeparator(0) + table.mkString(listSeparator(2)) + listSeparator(1))
  def add(key: String, table: Array[String]): Unit = insert(key, listSeparator(0) + textSeparator + table.mkString(textSeparator + listSeparator(2) + textSeparator) + textSeparator + listSeparator(1))
  def add(key: String, value: JSONBuilder): Unit = {
    val els = value.toString(indent + 1).split("\n")
    insert(key, els.mkString(if(els.length > 3) "\n" else " "))
  }

  def tab(indent: Int): String = {
    tabulation * indent
  }
  def toString(ind: Int): String = {
    indent = ind
    "{" + lineSeparator +
      content.map(tab(indent) + _).mkString(contentSeparator) + lineSeparator +
    tab(indent-1) + "}"
  }
  override def toString(): String = {
    toString(indent)
  }

}
