package utils


import scala.util.Random

/**
  * Created by vbranders on 17/04/17.
  *
  * Generate a range of synthetic matrices with provided parameters.
  *
  * args(0) path to the folder that will contain the generated matrix; mandatory; string
  * args(1) starting value of the seed range for the generated matrices; default to 0; integer
  * args(2) finishing (not included) value of the seed range for generated matrices; default to 1; integer
  * args(3) probability for each column to be in the implanted sub-matrix; default to 0.2; float
  * args(4) number of rows; default to 10,000; integer
  * args(5) number of columns; default to 100; integer
  * args(6) average value of the low distribution (the non implanted sub-matrix); default to -3; float
  * args(7) average value of the high distribution (the implanted sub-matrix); default to 1; float
  * args(8) standard deviation of the low and high distributions; default to 1; float
  * args(9) probability for each row to be in the implanted sub-matrix; default to value of args(3); float
  * args(10) number of sub-matrix to implant; default to 1; integer
  *
  */
object MatrixBuilder extends App {

  var lineSeparator = sys.props("line.separator")
  val valueSeparator: String = "\t"

  val storeParametersName: String = "_parameters.JSON"
  val storeMatrixName: String = "_matrix.tsv"

  val storePath: String = args(0)
  val seedStart: Int = defaultOrArg(1, 0)
  val seedEnd: Int = defaultOrArg(2, 0)
  val pSol: Float = defaultOrArg(3, 0.2f)
  val nRows: Int = defaultOrArg(4, 10000)
  val nCols: Int = defaultOrArg(5, 100)
  val lowAverage: Float = defaultOrArg(6, -3f)
  val highAverage: Float = defaultOrArg(7, 1f)
  val standardDeviation: Float = defaultOrArg(8, 1f)
  val pSolRow: Float = defaultOrArg(9, pSol)
  val nSol: Int = defaultOrArg(10, 1)

  var rand: Random = new Random(seedStart)

  for(seed <- seedStart until seedEnd){
    rand = new Random(seed)

    // Background noise = low average distribution
    val matrix: Array[Array[Double]] = Array.tabulate(nRows, nCols)((i, j) => sampleGaussian(lowAverage, standardDeviation))

    // Implanted sub-matrix = high average distribution
    val tRows: Array[Array[Int]] = Array.tabulate(nSol)(n => Array.tabulate[Int](nRows)(i => i).filter(i => rand.nextDouble() < pSolRow))
    val tCols: Array[Array[Int]] = Array.tabulate(nSol)(n => Array.tabulate[Int](nCols)(j => j).filter(j => rand.nextDouble() < pSol))

    for(s <- 0 until nSol) {
      for (r <- tRows(s); c <- tCols(s)) {
        matrix(r)(c) = sampleGaussian(highAverage, standardDeviation)
      }
    }
    val coverage: Double = (for(s <- 0 until nSol) yield (tRows(s).size * tCols(s).size).toDouble ).sum / (nRows * nCols).toDouble
    val effectiveCoverage: Double = (matrix.map(_.filter(_ >= 0).size).sum).toDouble / (nRows * nCols).toDouble

    // Store parameters and matrix
    new FileSaver(getParameters(seed, coverage, effectiveCoverage, tRows, tCols), storePath + seed + storeParametersName)
    new FileSaver(matrix.map(_.mkString(valueSeparator)).mkString(lineSeparator), storePath + seed + storeMatrixName)

  }

  def sampleGaussian(mean: Float, sd: Float): Double = mean + (rand.nextGaussian() * sd)

  def getParameters(seed: Int, coverage: Double, effectiveCoverage: Double, tRows: Array[Array[Int]], tCols: Array[Array[Int]]): String = {
    val json: JSONBuilder = new JSONBuilder()
    json.add("seed", seed)
    json.add("pSol", pSol)
    if(pSol != pSolRow) {
      json.add("pSolRow", pSolRow)
    }
    json.add("nRows", nRows)
    json.add("nCols", nCols)
    json.add("coverage", coverage)
    json.add("effectiveCoverage", effectiveCoverage)
    json.add("trueRows", "["+tRows.map(_.mkString(",")).mkString("];[")+"]")
    json.add("trueCols", "["+tCols.map(_.mkString(",")).mkString("];[")+"]")
    json.add("lowAverage", lowAverage)
    json.add("highAverage", highAverage)
    json.add("standardDeviation", standardDeviation)
    json.toString()
  }

  def defaultOrArg(arg: Int, default: String): String = if(args.size > arg && args(arg).size > 0) args(arg) else default
  def defaultOrArg(arg: Int, default: Int): Int = defaultOrArg(arg, default.toString).toInt
  def defaultOrArg(arg: Int, default: Float): Float = defaultOrArg(arg, default.toString).toFloat
}
