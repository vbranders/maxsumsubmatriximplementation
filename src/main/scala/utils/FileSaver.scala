package utils

import java.io.{File, FileOutputStream, IOException, PrintWriter}

/**
  * Created by vbranders on 17/04/17.
  *
  * Save a string to a file in append or overwrite mode.
  * The saved file is either empty or non empty finishing with a new line.
  *
  */
class FileSaver(val str: String, val path: String, val append: Boolean) {
  def this(str: String, path: String) = this(str, path, false)

  try {
    val f: File = new File(path)
    f.getParentFile.mkdirs()
    val writer = new PrintWriter(new FileOutputStream(f, append))
    writer.write(str)
    if(!str.endsWith(sys.props("line.separator")) && str.length > 0){
      writer.write(sys.props("line.separator"))
    }
    writer.close()
  } catch {
    case e: IOException => e.printStackTrace()
  }
}
