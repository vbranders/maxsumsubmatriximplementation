package mssm

import java.util.Calendar

import utils.{FileSaver, JSONBuilder}

import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
  * Created by vbranders on 18/04/17.
  *
  * Save all informations of identified solutions
  *
  */
trait SolutionFinder extends App {

  private val TIMES: ArrayBuffer[Long] = new ArrayBuffer[Long]() // A table of the time elapsed since the start of the search. For each new solution found an new entry is added to the table
//  private val SCORES: ArrayBuffer[Double] = new ArrayBuffer[Double]() // A table of the objective value of the best solutions found. For each new solution an new entry is added to the table

  protected var completed: Boolean = false // A flag indicating whether the best solution has been proved to be optimal (true) or not
  protected var failed: Boolean = true // A flag indicating whether the run completed without problem (memory issue, errors, ...) by proving optimality or reaching the maximum allocated time budget
  protected var interrupt: Boolean = false

  protected var requiredTime: Long = 0l // The time (in nanoseconds) before completion of the search

  protected val valueSeparator: String = "\t"

  protected var cnt: Int = 0
  private var time: Long = System.nanoTime()
  protected var verbose: Boolean = false

  def shouldInterrupt(): Boolean = interrupt
  def forceInterrupt(): Unit = {interrupt = true;println("Interruption required !")}
  def releaseInterrupt(): Unit = {interrupt = false}

  /**
    * Prepare the solution file
    */
  def initSol(): Unit = {
    //TODO: commented for new paper !
//    new FileSaver("#" + getName(), getNameSolution(), false)
//    new FileSaver("#" + getDescription(), getNameSolution(), true)
//    new FileSaver("#" + getFileName(), getNameSolution(), true)
//    new FileSaver("#Count\tTime\tCols", getNameSolution(), true)
  }
  /**
    * Store the new solution to the file
    */
  def addSolution(cols: Array[Int]): Unit = {
    cnt += 1
    TIMES.append(System.nanoTime() - time)
    //TODO: commented for new paper !
//    new FileSaver(cnt + "\t" + (System.nanoTime() - time) + "\t"+cols.mkString(","), getNameSolution(), true)//cnt>1)
  }

  /**
    * @return the name of the method used to identify a solution
    */
  def getName(): String

  /**
    * @return the description of the mehod used to identify a solution
    */
  def getDescription(): String

//  /**
//    * @return an array of the time elapsed since the start of the search for each new solution found
//    */
//  def getTimeOfSolutions(): Array[Long] = TIMES.toArray

//  /**
//    * @return an array of the value of the objective function for each new solution found
//    */
//  def getScoreOfsolutions(): Array[Double] = {//SCORES.toArray
//    val originalMatrix = Source.fromFile(getFileName()).getLines().toArray.map(_.split(valueSeparator).map(_.toDouble))
//    val nRow: Int = originalMatrix.size
//    val nCol: Int = originalMatrix(0).size
//
//    val sols: Array[Array[Int]] = Source.fromFile(getNameSolution()).getLines().filter(_.size > 1).toArray.map(_.split("\\[")(1).split("\\]")(0).split(",").map(_.toInt))
//    var scores: Array[Double] = Array.ofDim[Double](sols.size)
//    for(k <- 0 until sols.size){
//      var sol: Double = 0d
//      for(i <- 0 until nRow){
//        var sumRow = 0d
//        for(j <- sols(k)){
//          sumRow += originalMatrix(i)(j)
//        }
//        if(sumRow > 0){
//          sol += sumRow
//        }
//      }
//      scores(k) = sol
//    }
//    scores
//  }

//  def getCoverage(): Array[Double] = {
//    val originalMatrix = Source.fromFile(getFileName()).getLines().toArray.map(_.split(valueSeparator).map(_.toDouble))
//    val nRow: Int = originalMatrix.size
//    val nCol: Int = originalMatrix(0).size
//
//    val sols: Array[Array[Int]] = Source.fromFile(getNameSolution()).getLines().filter(_.size > 1).toArray.map(_.split("\\[")(1).split("\\]")(0).split(",").map(_.toInt))
//    var covs: Array[Double] = Array.ofDim[Double](sols.size)
//    for(k <- 0 until sols.size){
//      var cnt: Int = 0
//      for(i <- 0 until nRow){
//        var sumRow = 0d
//        for(j <- sols(k)){
//          sumRow += originalMatrix(i)(j)
//        }
//        if(sumRow > 0){
//          cnt += sols(k).size
//        }
//      }
//      covs(k) = cnt.toDouble/(nRow*nCol).toDouble
//    }
//    covs
//
//  }

//  def getEffectiveCoverage(): Array[Double] = {
//    val originalMatrix = Source.fromFile(getFileName()).getLines().toArray.map(_.split(valueSeparator).map(_.toDouble))
//    val nRow: Int = originalMatrix.size
//    val nCol: Int = originalMatrix(0).size
//
//    val sols: Array[Array[Int]] = Source.fromFile(getNameSolution()).getLines().filter(_.size > 1).toArray.map(_.split("\\[")(1).split("\\]")(0).split(",").map(_.toInt))
//    var covs: Array[Double] = Array.ofDim[Double](sols.size)
//    for(k <- 0 until sols.size){
//      var cnt: Int = 0
//      var cntSel: Int = 0
//      for(i <- 0 until nRow){
//        var sumRow = 0d
//        var cntRow: Int = 0
//        for(j <- sols(k)){
//          sumRow += originalMatrix(i)(j)
//          if(originalMatrix(i)(j) >= 0){
//            cntRow += 1
//          }
//        }
//        if(sumRow > 0){
//          cnt += cntRow
//          cntSel += sols(k).size
//        }
//      }
//      covs(k) = cnt.toDouble/cntSel.toDouble
//    }
//    covs
//
//  }

//  def getSel(): Array[Array[Double]] = {
//    val originalMatrix = Source.fromFile(getFileName()).getLines().toArray.map(_.split(valueSeparator).map(_.toDouble))
//    val nRow: Int = originalMatrix.size
//    val nCol: Int = originalMatrix(0).size
//
//    var nbPos: Int = 0
//    var nbNeg: Int = 0
//    for(i <- 0 until nRow){
//      for(j <- 0 until nCol){
//        if(originalMatrix(i)(j) >= 0){
//          nbPos += 1
//        } else {
//          nbNeg += 1
//        }
//      }
//    }
//
//    val sols: Array[Array[Int]] = Source.fromFile(getNameSolution()).getLines().filter(_.size > 1).toArray.map(_.split("\\[")(1).split("\\]")(0).split(",").map(_.toInt))
//    var sels: Array[Array[Double]] = Array.ofDim[Double](sols.size, 5)
//    for(k <- 0 until sols.size){
//      var cntPos: Int = 0
//      var cntNeg: Int = 0
//      for(i <- 0 until nRow){
//        var sumRow = 0d
//        var cntRowPos: Int = 0
//        var cntRowNeg: Int = 0
//        for(j <- sols(k)){
//          sumRow += originalMatrix(i)(j)
//          if(originalMatrix(i)(j) >= 0){
//            cntRowPos += 1
//          } else {
//            cntRowNeg += 1
//          }
//        }
//        if(sumRow > 0){
//          cntPos += cntRowPos
//          cntNeg += cntRowNeg
//        }
//      }
//      val precision: Double = cntPos.toDouble/(cntPos + cntNeg).toDouble
//      val recall: Double = cntPos.toDouble/nbPos.toDouble
//      sels(k) = Array((cntPos - (nbPos - cntPos)).toDouble/(nbPos.toDouble), ((nbNeg - cntNeg) - cntNeg).toDouble/(nbNeg.toDouble), precision, recall, 2*((precision*recall)/(precision+recall)))
//    }
//    sels
//
//  }

//  def getF1(): Array[Array[Double]] = {
//    val rows: Array[Int] = Source.fromFile(getFileName().substring(0, getFileName().lastIndexOf("_matrix")) + "_parameters.JSON").getLines().filter(_.contains("trueRows")).toArray.apply(0).split("\\[")(1).split("\\]")(0).split(", ").map(_.toInt)
//    val cols: Array[Int] = Source.fromFile(getFileName().substring(0, getFileName().lastIndexOf("_matrix")) + "_parameters.JSON").getLines().filter(_.contains("trueCols")).toArray.apply(0).split("\\[")(1).split("\\]")(0).split(", ").map(_.toInt)
//
//    val originalMatrix = Source.fromFile(getFileName()).getLines().toArray.map(_.split(valueSeparator).map(_.toDouble))
//    val nRow: Int = originalMatrix.size
//    val nCol: Int = originalMatrix(0).size
//
//    val sols: Array[Array[Int]] = Source.fromFile(getNameSolution()).getLines().filter(_.size > 1).toArray.map(_.split("\\[")(1).split("\\]")(0).split(",").map(_.toInt))
//    var quality: Array[Array[Double]] = Array.ofDim[Double](sols.size, 3)
//    for(k <- 0 until sols.size){
//      var TPr: Int = 0
//      var FPr: Int = 0
//      var TPc: Int = 0
//      var FPc: Int = 0
//      var cntR: Int = 0
//      for(i <- 0 until nRow){
//        var sumRow = 0d
//        for(j <- sols(k)){
//          sumRow += originalMatrix(i)(j)
//        }
//        if(sumRow > 0){
//          cntR += 1
//          if(rows.contains(i)){
//            TPr += 1
//          } else {
//            FPr += 1
//          }
//        }
//      }
//      for(j <- sols(k)){
//        if(cols.contains(j)){
//          TPc += 1
//        } else {
//          FPc += 1
//        }
//      }
//      val precision: Double = (TPr * TPc).toDouble / (cntR * sols(k).size)
//      val recall: Double = (TPr * TPc).toDouble / (rows.size * cols.size)
//      val f1: Double = 2*(precision * recall)/(precision + recall)
//      quality(k) = Array(precision, recall, f1)
//    }
//    quality
//  }


  /**
    * @return the time elapsed since the start of the search until the completion (proving optimality)
    */
  def getTimeToComplete(): Long = requiredTime

  /**
    * @return the time elapsed since the start of the search until the identification of the best solution found
    */
  def getTimeBest(): Long = {
    if(TIMES.size > 0) {
      TIMES.last
    } else {
      0l
    }
  }

  /**
    * @return the value of the objective function of the best solution found
    */
  def getScoreBest(): Double

  /**
    * @return a boolean testing whether the search proved optimality (true) or not
    */
  def isCompleted(): Boolean = completed

  /**
    * @return a boolean testing whether the search has been interrupter for any reason other than time (memory issues, errors, ...)
    */
  def isFailed(): Boolean = failed

  /**
    * By default, a failed state is considered. The state is switched (unfailed) only if the run is completed or the maximum budget of time is reached
    *
    * @param f a boolean stating whether the solution has been failed (true) or not
    */
  def setFailed(f: Boolean): Unit = {if(f){println("Run failed !")}; failed = f}

  /**
    * @return the maximum allocated time budget to find a solution (and possibly prove optimality)
    */
  def getTimeBudget(): Long

  /**
    * @return the path to store the solution(s) informations
    */
  def getFileName(): String

  /**
    * Store the results of the search in a JSON format
    */
  def storeJSON(): Unit = {
    val r: JSONBuilder = new JSONBuilder()
    r.add("name", getName())
    r.add("descr", getDescription())
    r.add("completed", isCompleted().toString)
    r.add("failed", isFailed().toString)
    r.add("best", getScoreBest())
    r.add("timeToBest", getTimeBest())
    r.add("timeToStop", getTimeToComplete())
    r.add("timeBudget", getTimeBudget())
//    r.add("times", getTimeOfSolutions())
//    r.add("scores", getScoreOfsolutions())
//    r.add("coverage", getCoverage())
//    r.add("coverageEffective", getEffectiveCoverage())
//    r.add("Quality", getSel().map(_.mkString(",")))
//    r.add("f1", getF1().map(_.mkString(",")))
    //TODO: commented for new paper
//    new FileSaver(r.toString(), getNameJSON())
  }
  def storeSol(rows: Array[Int], cols: Array[Int], path: String): Unit = {
    new FileSaver(cols.filter(_ > 0).mkString(" "), path + "_cols.txt", false)
    new FileSaver(rows.filter(_ > 0).mkString(" "), path + "_rows.txt", false)
  }
  def storeColumn(rows: Array[Int], cols: Array[Int], path: String): Unit = {
//    println(rows.length)
//    println(rows.mkString("\t"))
//    println(cols.length)
//    println(cols.mkString("\t"))
//    println(Array.tabulate(rows.length)(i => Array.tabulate(cols.length)(j => if (rows(i) * cols(j) == 1) 1 else 0).mkString("\t")).mkString("\t"))
    new FileSaver(rows.mkString("\t"), path+".T")
    new FileSaver(cols.mkString("\t"), path+".U")
//    new FileSaver(Array.tabulate(rows.length)(i => Array.tabulate(cols.length)(j => if (rows(i) * cols(j) == 1) 1 else 0).mkString("\t")).mkString("\t"), path)
  }

  def getNameJSON(): String = getFileName().substring(0, getFileName().lastIndexOf('.')) + '/' + getName() + "/solution.JSON"
  def getNameSolution(): String = getFileName().substring(0, getFileName().lastIndexOf('.')) + '/' + getName() + "/solution.log.txt"

  def defaultOrArg(arg: Int, default: String): String = if(args.size > arg && args(arg).size > 0) args(arg) else default
  def defaultOrArg(arg: Int, default: Int): Int = defaultOrArg(arg, default.toString).toInt
  def defaultOrArg(arg: Int, default: Boolean): Boolean = defaultOrArg(arg, default.toString).toBoolean
  def defaultOrArg(arg: Int, default: Float): Float = defaultOrArg(arg, default.toString).toFloat

  def printinit(): Unit = {
    initSol()
    if(verbose) {
      print("Starting *\t")
      val cal = Calendar.getInstance()
      print("Time: ")
      print(cal.get(Calendar.HOUR_OF_DAY) + "h, ")
      print(cal.get(Calendar.MINUTE) + "m, ")
      println(cal.get(Calendar.SECOND) + "s.")
    }
    time = System.nanoTime()
  }
}
