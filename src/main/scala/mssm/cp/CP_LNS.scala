package mssm.cp

import mssm.SolutionFinder
import oscar.cp._
import oscar.cp.core.CPSolver
import oscar.cp.core.variables.CPBoolVar

import scala.io.Source

/**
  * Created by vbranders on 20/04/17.
  *
  */
object CP_LNS extends SolutionFinder {

  private val filePath = args(0)
  private val timeMax = defaultOrArg(1, 10).toLong * 1000 * 1000 * 1000 // maximum allocated time in nanoseconds
  private val fileComplement = defaultOrArg(2, "")
  private val useLNS = defaultOrArg(3, true)
  private val useIntegerMatrix = defaultOrArg(4, false)
  private val useLeVanModel = defaultOrArg(5, false)


  private val methodName = "CP" + (if(useLNS) "-LNS" else "") + (if(useLeVanModel) "_LeVan" else "")
  private val methodDescr = {
    "Constraint Programming" + (if(useLNS) " using large neighborhood search" else "") + (if(useLeVanModel) " with redundant constraints (model proposed by Le Van et al. (2014)." else " with improved model (similar filtering to the proposition of Le Van et al. (2014) but lighter filtering)")
  }

  def getName(): String = methodName + fileComplement
  def getDescription(): String = methodDescr
  def getScoreBest(): Double = bestScore
  def getTimeBudget(): Long = timeMax
  def getFileName(): String = filePath


  // ---------------------------------------- Reading the matrix ----------------------------------------
  val originalMatrix: Array[Array[Double]] = Source.fromFile(filePath).getLines().toArray.map(_.split(valueSeparator).map(_.toDouble))
  val nRow: Int = originalMatrix.size
  val nCol: Int = originalMatrix(0).size

  var mapFactor: Double = 0d
  val useMatrix: Array[Array[Int]] = {
    if(useIntegerMatrix) {
      originalMatrix.map(_.map(_.toInt))
    } else {
      // Map matrix values to integer scale
      val largest = originalMatrix.map(_.max).max
      val smallest = originalMatrix.map(_.min).min
      mapFactor = {
        Math.min((Int.MaxValue.toDouble / (nRow * nCol))/largest, (Int.MinValue.toDouble / (nRow * nCol))/smallest)
      }
      originalMatrix.map(_.map(m => (m * mapFactor).toInt))
    }
  }


  // ---------------------------------------- CP Model ----------------------------------------
  var bestScore: Double = Double.MinValue
  println("root UB:" + originalMatrix.map(_.filter(_ > 0).sum).sum)

  implicit val cp = CPSolver()

  val x = Array.fill[CPBoolVar](nCol)(CPBoolVar())
  val y = Array.fill[CPBoolVar](nRow)(CPBoolVar())

  val xBest = Array.ofDim[Int](nCol)

  val xsorted = {
    val matrixRevert = Array.tabulate(nCol)(j => Array.tabulate(nRow)(i => originalMatrix(i)(j)))
    val sumCol = Array.tabulate(nCol)(j => matrixRevert(j).filter(_ > 0).sum)
    val permSumCol = (0 until nCol).sortBy(j => -sumCol(j))
    Array.tabulate(nCol)(i => x(permSumCol(i)))
  }

  printinit()
  private val time: Long = System.nanoTime()

  val objective = {
    if(useLeVanModel){
      /* Model of Le Van et al.(2014). (less efficient)
       */
      for(i <- 0 until nRow){
        add((sum(0 until nCol)(j => x(j) * useMatrix(i)(j)) ?>= 0) === y(i))
      }
      for(j <- 0 until nCol){
        add((sum(0 until nRow)(i => y(i) * useMatrix(i)(j)) ?>= 0) === x(j))
      }
      sum(for(i <- 0 until nRow; j <- 0 until nCol) yield (x(j) && y(i)) * useMatrix(i)(j))
    } else {
      val sumLine = Array.tabulate(nRow)(i => sum(0 until nCol)(j => x(j) * useMatrix(i)(j)))
      sum(Array.tabulate(nRow)(i => (sumLine(i) ?>= 0) * sumLine(i)))
    }
  }

  maximize(objective)

  cp.search{
    binaryStaticIdx(xsorted, i => xsorted(i).max)
  }

  onSolution{
//    TIMES.append(System.nanoTime() - time)
    // Record the best solution
    for(i <- 0 until x.size){
      if(x(i).isTrue) xBest(i) = 1
      else xBest(i) = 0
    }
    addSolution((0 until x.size).filter(i => xBest(i) == 1))
    bestScore = {
      if(useIntegerMatrix){
        objective.value
      } else {
        var sol: Double = 0d
        for(i <- 0 until nRow){
          var sumRow = 0d
          for(j <- 0 until nCol){
            if(xBest(j) == 1){
              sumRow += originalMatrix(i)(j)
            }
          }
          if(sumRow > 0){
            sol += sumRow
          }
        }
        sol
      }
    }
//    SCORES.append(bestScore)
    println("new best bound:" + bestScore + "\tTime (nanoseconds): " + java.text.NumberFormat.getIntegerInstance.format(((System.nanoTime()-time)).toInt))
  }

  if(useLNS){
    // Find a first solution to initialize LNS with limited number of backtracks
    val stat = cp.start(failureLimit = 100)
    val rand = new scala.util.Random(0)
    var i = 0
    while(true){
      // Large Neighorhood Search
      i += 1
      val stat = cp.startSubjectTo(failureLimit = 100){
        cp.add((0 until nCol).filter(i => rand.nextInt(100) < 50).map(i => x(i) === xBest(i)))
      }
    }
  } else {
    val stat = cp.start()
  }

  requiredTime = System.nanoTime() - time
  completed = true
  failed = false
  storeJSON()

}
