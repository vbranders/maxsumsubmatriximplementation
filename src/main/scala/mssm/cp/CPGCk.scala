package mssm.cp

import mssm.SolutionFinder
import oscar.cp._
import oscar.cp.core.CPSolver
import oscar.cp.core.variables.CPBoolVar
import utils.ArgsParser

import scala.io.Source

/**
  * @author Pierre Schaus
  *
  */
object CPGCk extends SolutionFinder {

  val cmd = Map(
    "-t" -> Array("Int", "10", "Budget limit in seconds"),
    "-n" -> Array("String", "", "Name of experiment (included in stored files)"),
    "-lf" -> Array("Boolean", "false", "Use of light filter"),
    "-lns" -> Array("Int", "0", "Number of non-improving iterations before interrupting (-1 for infinite loop, 0 for complete search)"),
    "-h" -> Array("Boolean", "true", "Heuristic reordering of columns"),
    "-lb" -> Array("Double", "0", "Starting lower bound"),
    "-ns" -> Array("Int", Int.MaxValue.toString, "Number of solutions before interrupting"),
    "-v" -> Array("Int", "0", "Verbose (Yes:1|No:0)"),
    "-c" -> Array("Int", "0", "Store solution as column (Yes:1|No:0)"),
    "-k" -> Array("Int", "1", "Number of non-overlapping solutions to find")
  )

  val parser = new ArgsParser()
  val res = parser.parse(cmd, args.drop(1))


  //println(args.mkString(" "))
  private val filePath = args(0)
  private val timeMax = res("-t").toLong * 1000 // maximum allocated time in milliseconds
//  private val timeMax = defaultOrArg(1, 10).toLong * 1000 * 1000 * 1000 // maximum allocated time in nanoseconds
  private val fileComplement = res("-n")//defaultOrArg(2, "")
  private val lightFilter = res("-lf").toBoolean//defaultOrArg(3, false)
  private val useLNS = res("-lns") != "0"//defaultOrArg(4, false)
  private val nbLNS = res("-lns").toInt
  private val useHeuristic = res("-h").toBoolean//defaultOrArg(5, true)
  private val minValue = res("-lb").toDouble//defaultOrArg(6, "0.0d").toDouble
  private val nSols = res("-ns").toInt
  verbose = res("-v") == "1"
  private val storeAsCol = res("-c") == "1"
  private val nSolK = res("-k").toInt

  private val methodName = "CPGCk" //+ (if(lightFilter) "_lightFilter" else "") + (if(useLNS) "_lns" else "")+ (if(useHeuristic) "" else "_noHeuristic")
  private val methodDescr = {
    "Constraint Programming with Global Constraint approach" + (if(useLNS) " using large neighborhood search" else "")
  }

  def getName(): String = methodName + fileComplement
  def getDescription(): String = methodDescr
  def getScoreBest(): Double = bestScore
  def getTimeBudget(): Long = timeMax
  def getFileName(): String = filePath


  // ---------------------------------------- Reading the matrix ----------------------------------------
  val originalMatrix = Source.fromFile(filePath).getLines().toArray.map(_.split(valueSeparator).map(_.toDouble))


  val nRow: Int = originalMatrix.size
  val nCol: Int = originalMatrix(0).size


  // ---------------------------------------- CP Model ----------------------------------------
  var bestScore: Double = Double.MinValue
  if(verbose) {
    println("root UB:" + originalMatrix.map(_.filter(_ > 0).sum).sum)
  }

  implicit val cp = CPSolver()

  val x = Array.fill[CPIntVar](nCol)(CPIntVar(0 to nSolK))

  val xBest = Array.ofDim[Int](nCol)



  // *********************** TEST
  // Sorting rows and columns based on easiness to detected which is best choice
  // *********************** TEST


  val xsorted = {
    if(useHeuristic) {
      val matrixRevert = Array.tabulate(nCol)(j => Array.tabulate(nRow)(i => originalMatrix(i)(j)))
      val sumCol = Array.tabulate(nCol)(j => matrixRevert(j).filter(_ > 0).sum)
      val permSumCol = (0 until nCol).sortBy(j => -sumCol(j))
      Array.tabulate(nCol)(i => x(permSumCol(i)))
    } else {
      Array.tabulate(nCol)(i => x(i))
    }
  }

  printinit()

  if(storeAsCol) {
    storeColumn(Array.tabulate(nRow)(i => 0), Array.tabulate(nCol)(i => 0), filePath + "_subproblem.cols.tsv")
  }
  private val time: Long = System.currentTimeMillis()

  val maxSumConstraint = new MaxSumSubMatrixConstraintk(x, originalMatrix, nSolK, lightFilter)
  maxSumConstraint.setMinValue(minValue)
  cp.add(maxSumConstraint)

  maxSumConstraint.onSolution = () => {
//    TIMES.append(System.nanoTime() - time)
    bestScore = maxSumConstraint.bestBound
//    SCORES.append(bestScore)
    // record best solution
    var cnt = 0
    for (i <- 0 until x.size) {
      if (x(i).isBound && x(i).value != 0) {xBest(i) = x(i).value; cnt += 1}
      else xBest(i) = 0
    }
    println(xBest.mkString(","))
    addSolution((0 until x.size).filter(i => xBest(i) == 1))
    if(storeAsCol) {
      storeColumn((0 until nRow).map(i => if ((0 until x.size).map(j => if (xBest(j) == 1) originalMatrix(i)(j) else 0).sum > 0) {
        1
      } else {
        0
      }), (0 until x.size).map(i => if (xBest(i) == 1) 1 else 0), filePath + "_subproblem.cols.tsv")
    }
    maxSumConstraint.nSols += 1

    if(verbose) {
      println("nbb: " + bestScore + "\tCols: " + cnt + "\t\tTime (milliseconds): " + java.text.NumberFormat.getIntegerInstance.format(((System.currentTimeMillis() - time)).toInt))
    }
  }


  cp.search {
    binaryStaticIdx(xsorted, i => xsorted(i).max)
  }


  def stopCondition: Boolean = {
    maxSumConstraint.nSols >= nSols
  }

  if (useLNS) {
    println("Use LNS")
    // Find first solution to initialize LNS with limited number of backtracks
    val stat = cp.start(failureLimit = 500, maxDiscrepancy = 2)
    val rand = new scala.util.Random(0)
    var i = 0
    for(r <- 1 to nbLNS){
      if(verbose){
        println("LNS: " + r + "/" + nbLNS)
      }
//    while(true){
      // Large Neighorhood Search
      i += 1
      val stat = cp.startSubjectTo(failureLimit = 500, maxDiscrepancy = 2) {
        cp.add((0 until nRow).filter(i => rand.nextInt(100) < 80).map(i => x(i) === xBest(i)))
      }
    }
  } else {
    println("Use complete search")
    val stat = cp.start(stopCondition = stopCondition)
//    println(stat)
  }

  requiredTime = System.currentTimeMillis() - time
  completed = true
  failed = false
  storeJSON()
  println("Done")
}
