package mssm.cp

import oscar.algo.Inconsistency
import oscar.algo.reversible.ReversibleInt
import oscar.cp._
import oscar.cp.core.CPPropagStrength
import oscar.cp.core.variables.{CPBoolVar, CPVar}

/**
  * Global Constraint also doing the branch and bound internally as at every level of decision
  * we have a feasible decision.
  */
class MaxSumSubMatrixConstraintk(x: Array[CPIntVar], matrix: Array[Array[Double]], nSolK: Int, lightFilter: Boolean) extends Constraint(x(0).store) {

  var onSolution: () => Unit = () => {}


  val n = matrix.size
  val m = matrix(0).size

  val partialSum = Array.tabulate(nSolK, n)((j, i) => new ReversibleDouble(s, 0.0)) // Array of rows partial sums for each tile

  // Reversible sparse-set trick to maintain unbound variables
  val unBound = Array.tabulate(m)(i => i)
  val nUnBound = new ReversibleInt(s, m)

  // Reversible sparse-set trick to maintain possibly interesting lines
  val candidate = Array.tabulate(n)(i => i)
  val nCandidate = new ReversibleInt(s, n)

  val xy = Array.fill[Int](n)(0)

  // TODO: maintain tile nuber for the selection

  var nSols = 0
  var bestBound = Double.MinValue

  def setMinValue(m: Double): Unit = {
    bestBound = m
  }

  def setup(l: CPPropagStrength): Unit = {
    // propagate method is called when any of the variables is bound
    for (i <- 0 until m) {
      x(i).callPropagateWhenBind(this);
    }
    propagate()
  }

  // Evaluate
  def removeBoundVariablesAndUpdatePartialSum(): Unit = {
    var size = nUnBound.value
    var j = size
    while (j > 0) {
      j -= 1
      if (x(unBound(j)).isBound) {
        size -= 1
        val tmp = unBound(j)
        if(x(tmp).value != 0) {
          select(tmp, x(tmp).value)
        }
        unBound(j) = unBound(size)
        unBound(size) = tmp
//        val tmp = unBound(j)
//        if (x(tmp).isTrue) select(tmp)
//        unBound(j) = unBound(size)
//        unBound(size) = tmp
      }
    }
    nUnBound.value = size
  }

  // Update partial sums
  def select(c: Int, d: Int): Unit = {
    var i = nCandidate.value
    while (i > 0) {
      i -= 1
      partialSum(d-1)(candidate(i)) += matrix(candidate(i))(c)
    }
  }

  def selectedRows(): Array[Int] = {
    candidate.take(nCandidate).filter(i => Array.tabulate(nSolK)(j => partialSum(j)(i).value).max > 0)
  }

  def computeObjective(): Double = {
    var objective = 0.0
    var i = nCandidate.value
    while (i > 0) {
      i -= 1
      var pSum = Array.tabulate(nSolK)(j => partialSum(j)(candidate(i)).value).max //partialSum(candidate(i)).value
      if (pSum > 0) {
        objective += pSum
        xy(i) = (Array.tabulate(nSolK)(j => partialSum(j)(candidate(i)).value).zipWithIndex.maxBy(_._1)._2)+1
      }
    }
    return objective
  }


  // TODO: prunelight for multiple tiles
  def pruneLight(): Unit = {
    var objectiveUB = 0.0
    var nCand = nCandidate.value
    var i = nCand
    while (i > 0) {
      i -= 1
      var costLineUB = Array.tabulate(nSolK)(k => partialSum(k)(candidate(i)).value)
      // optimistic selection of positive values only for the UB
      var j = nUnBound.value
      while (j > 0) {
        j -= 1
        if (matrix(candidate(i))(unBound(j)) > 0) {
          for(k <- 0 until nSolK) {
            costLineUB(k) += matrix(candidate(i))(unBound(j))
          }
        }
      }
      if (costLineUB.max > 0) {
        objectiveUB += costLineUB.max
      } else {
        // remove this line as it can never become positive under this node
        nCand -= 1
        val tmp = candidate(i)
        candidate(i) = candidate(nCand)
        candidate(nCand) = tmp
      }
    }
    nCandidate.value = nCand
    // Pruning of the branch and bound
    if(objectiveUB <= bestBound){
      throw Inconsistency
    }
  }

  var objectiveUBNotSelect = Array.fill[Double](m, nSolK)(0.0)
  var objectiveUBForceSelect = Array.fill[Double](m, nSolK)(0.0)


  def prune(): Unit = {

    var objectiveUB = 0.0
    val nCols = nUnBound.value
    var j: Int = nCols
    while (j > 0) {
      j -= 1
      objectiveUBNotSelect(unBound(j)) = Array.tabulate(nSolK)(k => 0.0)
      objectiveUBForceSelect(unBound(j)) = Array.tabulate(nSolK)(k => 0.0)
    }
    var maxs = Array.tabulate(m)(k => 0)

    var nCand = nCandidate.value
    var i = nCand
    while (i > 0) {
      i -= 1
      var costLineUB = Array.tabulate(nSolK)(k => partialSum(k)(candidate(i)).value)
      // optimistic selection of positive values only for the UB
      j = nCols
      while (j > 0) {
        j -= 1
        if (matrix(candidate(i))(unBound(j)) > 0) {
          for(k <- 0 until nSolK) {
            costLineUB(k) += matrix(candidate(i))(unBound(j))
          }
        }
      }
      if (costLineUB.max > 0) {
        objectiveUB += costLineUB.max

        // objectiveUBNotSelect(j) = upper bound if column j would not be selected
        j = nCols
        while (j > 0) {
          j -= 1
/*
          val objUBNotSelect = Array.tabulate(nSolK)(k => 0.0)
          val objUBDoSelect = Array.tabulate(nSolK)(k => 0.0)
          var overlap = 0
          for(k <- 0 until nSolK) {
            if (costLineUB(k) > 0) {
              overlap += 1
              if (matrix(candidate(i))(unBound(j)) > 0) {
                if (costLineUB(k) - matrix(candidate(i))(unBound(j)) > 0) {
                  objUBNotSelect(k) += costLineUB(k) - matrix(candidate(i))(unBound(j))
                }
                objUBDoSelect(k) += costLineUB(k)
              } else {
                objUBNotSelect(k) += costLineUB(k)
                if (costLineUB(k) + matrix(candidate(i))(unBound(j)) > 0) {
                  objUBDoSelect(k) += costLineUB(k) + matrix(candidate(i))(unBound(j))
                }
              }
            }
          }
          var maxnot = objUBNotSelect.zipWithIndex.maxBy(_._1)._2
          maxs(unBound(j)) = maxnot
//          objectiveUBNotSelect(unBound(j))(maxnot) += objUBNotSelect(maxnot)
          var maxdo = objUBDoSelect.zipWithIndex.maxBy(_._1)._2
//          objectiveUBForceSelect(unBound(j))(maxdo) += objUBDoSelect(maxdo)

          for(k <- 0 until nSolK) {
            objectiveUBNotSelect(unBound(j))(k) += objUBNotSelect(k)
            if(k != maxdo) {
              objectiveUBForceSelect(unBound(j))(k) += objUBDoSelect(k) //-objUBNotSelect(k)
            }else {
              objectiveUBForceSelect(unBound(j))(k) += objUBDoSelect(k)
            }
//            objectiveUBForceSelect(unBound(j))(k) += objUBDoSelect(k)
          }
//          objectiveUBForceSelect(unBound(j))(maxdo) += objUBDoSelect(maxdo)
*/

          for(k <- 0 until nSolK) {
            if (costLineUB(k) > 0) {
              if (matrix(candidate(i))(unBound(j)) > 0) {
                if (costLineUB(k) - matrix(candidate(i))(unBound(j)) > 0) {
                  objectiveUBNotSelect(unBound(j))(k) += costLineUB(k) - matrix(candidate(i))(unBound(j))
                }
                objectiveUBForceSelect(unBound(j))(k) += costLineUB(k)
              } else {
                objectiveUBNotSelect(unBound(j))(k) += costLineUB(k)
                if (costLineUB(k) + matrix(candidate(i))(unBound(j)) > 0) {
                  objectiveUBForceSelect(unBound(j))(k) += costLineUB(k) + matrix(candidate(i))(unBound(j))
                }
              }
            }
          }
        }

      } else {
        // remove this line as it can never become positive under this node
        nCand -= 1
        val tmp = candidate(i)
        candidate(i) = candidate(nCand)
        candidate(nCand) = tmp
      }

    }
    // Pruning of the branch and bound
    if (objectiveUB <= bestBound) {
      throw Inconsistency
    }
    j = nCols
    while (j > 0) {
      j -= 1
//      println("----")
//      println("Not " + unBound(j) + ", " + objectiveUBNotSelect(unBound(j)).sum + ", " +bestBound)
//      println(objectiveUBNotSelect(unBound(j)).mkString(", "))
      val maxForce = objectiveUBForceSelect(unBound(j)).zipWithIndex.maxBy(_._1)._2
      for(k <- 0 until nSolK) {
        if(k != maxForce) {
          objectiveUBForceSelect(unBound(j))(k) = objectiveUBNotSelect(unBound(j))(k)
        }
      }
//      println("Do " + unBound(j) + ", " + objectiveUBForceSelect(unBound(j)).sum + ", " +bestBound)
//      println(objectiveUBForceSelect(unBound(j)).mkString(", "))
//      println(x.map("("+_.mkString(", ")+")").mkString(", "))
//      println(candidate.take(nCandidate).mkString(", "))
      if (objectiveUBNotSelect(unBound(j)).sum <= bestBound || objectiveUBForceSelect(unBound(j)).sum < objectiveUBNotSelect(unBound(j)).sum) {
//        println("Remove 0")
        x(unBound(j)).removeValue(0)
//        x(unBound(j)).assign(maxs(unBound(j))+1)
      }
      if (objectiveUBForceSelect(unBound(j)).sum <= bestBound) {
//        println("FORCE REMOVE !!!!")
        x(unBound(j)).assign(0)
      }
/*      var ok = Array.tabulate(nSolK)(k => 0)
      var no = Array.tabulate(nSolK)(k => 0)
      for(k <- 0 until nSolK){
        if (objectiveUBNotSelect(unBound(j))(k) <= bestBound) {
          ok(k) = 1
//        } else {
//          x(unBound(j)).removeValue(k+1)
        }
        if (objectiveUBForceSelect(unBound(j))(k) <= bestBound) {
          no(k) = 1
//          x(unBound(j)).removeValue(k+1)
        }
      }
      if(ok.sum == 1){
        x(unBound(j)).assign((ok.zipWithIndex.maxBy(_._1)._2)+1)
      }
      if(no.sum == 1){
        x(unBound(j)).assign(0)
      }*/
//      if (objectiveUBNotSelect(unBound(j)).max < bestBound) {
//        println("For position " + unBound(j) + ", assign: " + (objectiveUBNotSelect(unBound(j)).sorted.mkString(", ")))
//        x(unBound(j)).assign((objectiveUBNotSelect(unBound(j)).zipWithIndex.maxBy(_._1)._2)+1)
//      }
//      if (objectiveUBForceSelect(unBound(j)).max < bestBound) {
//        println("For position " + unBound(j) + ", go no " + objectiveUBForceSelect(unBound(j)).mkString(", "))
//        x(unBound(j)).assign(0)
//      }
    }
    nCandidate.value = nCand
  }

  override def propagate(): Unit = {
    removeBoundVariablesAndUpdatePartialSum()
    val objective = computeObjective()
    if (objective > bestBound) {
      bestBound = objective
      onSolution()
    }
    if (lightFilter) {
      pruneLight()
    } else {
      prune()
    }
  }

  override def associatedVars(): Iterable[CPVar] = x

}
