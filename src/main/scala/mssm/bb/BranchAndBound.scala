package mssm.bb

import mssm.SolutionFinder

import scala.io.Source
import scala.collection.mutable

/**
  * Created by vbranders on 18/04/17.
  *
  * args(0) path to the matrix; mandatory; string
  * args(1) maximum allocated time budgets in seconds; default to 10; integer
  * args(2) complement of name for the resulting file generated; empty by default; string
  *
  */
//TODO: give description
object BranchAndBound extends SolutionFinder {

  private val filePath = args(0)
  private val timeMax = defaultOrArg(1, 10).toLong * 1000 * 1000 * 1000 // maximum allocated time in nanoseconds
  private val fileComplement = defaultOrArg(2, "")

  private val methodName = "B&B"
  private val methodDescr = {
    "Branch and Bound approach using a priority queue with priority P(x) = [UPPER_BOUND(x) * phi + (1 - phi) * OBJECTIVE_FUNCTION(x)] and phi = sum( max(0, m(i)(j)) ) / sum( max(-m(i)(j), m(i)(j)) ) "
  }

  def getName(): String = methodName + fileComplement
  def getDescription(): String = methodDescr
  def getScoreBest(): Double = bestSol.getOBJ()
  def getTimeBudget(): Long = timeMax
  def getFileName(): String = filePath


  // ---------------------------------------- Reading the matrix ----------------------------------------
  val originalMatrix = Source.fromFile(filePath).getLines().toArray.map(_.split(valueSeparator).map(_.toDouble))
  val nRow: Int = originalMatrix.size
  val nCol: Int = originalMatrix(0).size


  // ---------------------------------------- Variable and value ordering ----------------------------------------
  /**
    * Ordering of the columns by ascending total sum (sum over i's in R of m(i)(j))
    */
  val sortedCols = {
    val matrixRevert = Array.tabulate(nCol)(j => Array.tabulate(nRow)(i => originalMatrix(i)(j)))
    val sumCol = Array.tabulate(nCol)(j => matrixRevert(j).sum)
    (0 until nCol).sortBy(j => sumCol(j))
  }
  /**
    * Ordering of the rows by ascending total sum (sum over j's in C of m(i)(j))
    */
//  val sortedRows = Array.tabulate[Int](nRow)(i => i)
//  {
//    val sumRow = Array.tabulate(nRow)(i => originalMatrix(i).sum)
//    (0 until nRow).sortBy(i => sumRow(i))
//  }


  // ---------------------------------------- Pre-processing ----------------------------------------
  val cumSumRowRevert: Array[Array[Double]] = Array.ofDim[Double](nRow, nCol) // Matrix of the cumulative sum of entries on the rows, starting from last column. Entry in (a)(b) = sum over x in [b,...,nCol[ of m(a)(x)
  val cumSumRowRevertPos: Array[Array[Double]] = Array.ofDim[Double](nRow, nCol) // Matrix of the cumulative sum of positive entries on the rows, starting from last column. Entry in (a)(b) = sum over x in [b,///,nCol[ of max(0, m(a)(x))
  val curSumRow: Array[Double] = new Array[Double](nRow) // Running sum of each row for the selected columns
  val sumColPos: Array[Double] = new Array[Double](nCol) //TODO: explain

  val phi: Double = {
    var phiNeg: Double = 0d
    var entry: Double = 0d
    for(r <- 0 until nRow){
//      entry = originalMatrix(sortedRows(r))(sortedCols(nCol - 1))
      entry = originalMatrix((r))(sortedCols(nCol - 1))
      cumSumRowRevert(r)(nCol - 1) = entry
      cumSumRowRevertPos(r)(nCol - 1) = Math.max(0, entry)
      phiNeg += Math.max(-entry, 0)
      for(c <- nCol - 2 to 0 by -1){
//        entry = originalMatrix(sortedRows(r))(sortedCols(c))
        entry = originalMatrix((r))(sortedCols(c))
        cumSumRowRevert(r)(c) = cumSumRowRevert(r)(c + 1) + entry
        cumSumRowRevertPos(r)(c) = cumSumRowRevertPos(r)(c + 1) + Math.max(0, entry)
        phiNeg += Math.max(-entry, 0)
      }
    }
    val phiPos: Double = Array.tabulate(nRow)(i => cumSumRowRevertPos(i)(0)).sum
    phiPos/(phiPos + phiNeg)
  }


  // ---------------------------------------- Variables of the model ----------------------------------------
  val curSelRow: Array[Int] = Array.tabulate(nRow)(i => i) // Sparse-set of the selected/candidate rows
  var curSelRowNb: Int = nRow // Size of curSelRow sparse-set

  private var curSol: Candidate = _ // Current solution evaluated
  private var bestSol: Candidate = _ // Current incumbent

  private var queue: mutable.PriorityQueue[Candidate] = new mutable.PriorityQueue[Candidate]() // Priority queue of the pool of solution to process

  private var nbBranching: Int = 0 // Number of branching
  private var nbPruned: Int = 0 // Number of nodes pruned
  private var nbImprovements: Int = 0 // Number of improving solutions

  private var maxUpB: Double = 0d


  // ---------------------------------------- Initialization ----------------------------------------
  printinit()
  private val time: Long = System.nanoTime()

  // Start with full selection of columns
  curSol = bndAndObjInit()
  // Store the solution as the next node to process and as the best solution found so far
  updateBest(curSol)
  //updateBest(new Candidate(curSol.getUB(), 4165, curSol.getBNDED(), curSol.getSelectedCols()))
  store(curSol)
  compareToIncumbent(curSol)


  // ---------------------------------------- Search ----------------------------------------
  while(!queue.isEmpty && !shouldInterrupt()){
    branchNextNode()
  }
  requiredTime = System.nanoTime() - time
  if(!shouldInterrupt()) {
    completed = true
    failed = false
  }
  storeJSON()






  // ---------------------------------------- Methods ----------------------------------------
  def branchNextNode(): Unit = {
    // Get the node with max priority in the queue and branch it
    curSol = queue.dequeue()
    nbBranching += 1

    // Compute upper bound and objective value of the first branch and prepossess values for the other branches
    var colsSelected: Array[Int] = curSol.getSelectedCols() //TODO: could improves memory usage here
    curSol = firstBranch(curSol.getBNDED() + 1, colsSelected)
    compareToIncumbent(curSol)
    var diff: Double = curSol.getUB() - bestSol.getOBJ()
    if(diff > 0){
      queue.enqueue(curSol)
    } else if(-diff > sumColPos(curSol.getBNDED())){
      nbPruned += 1
      return
    }
    // Computes upper bound and objective value of the remaining branches except last one
    for(c <- curSol.getBNDED() + 1 until nCol - 1){
      // Previous unbound variables fixed to 0 is now swapped to 1
      colsSelected = colsSelected ++ Array(c - 1)
      curSol = branchWithSwap(c, colsSelected)
      compareToIncumbent(curSol)
      diff = curSol.getUB() - bestSol.getOBJ()
      if(diff > 0){
        queue.enqueue(curSol)
      } else if(-diff > sumColPos(c)){
        nbPruned += 1
        return
      }
    }
    // Computes upper bound and objective value of the last branch, which does not requires to be included in the queue has it has no unbound variables
    curSol = branchWithSwap(nCol - 1, colsSelected ++ Array(nCol - 2))
    compareToIncumbent(curSol)
  }


  def firstBranch(searchStart: Int, selected: Array[Int]) = {
    var upperBound: Double = 0d
    var objectiveValue: Double = 0d
    val complemented: Boolean = 2 * selected.length > searchStart+1
    // If there are more elements to add than to subtract
    if(complemented) {
      val complement: Array[Int] = new Array[Int](searchStart - selected.length)
      var selectedPos: Int = 0
      var complementPos: Int = 0
      for (c <- 0 until searchStart) {
        if (selectedPos >= selected.length || selected(selectedPos) != c) {
          complement(complementPos) = c
          complementPos += 1
        } else {
          selectedPos += 1
        }
      }
      for (r <- 0 until nRow) {
        curSumRow(r) = cumSumRowRevert(r)(0) - cumSumRowRevert(r)(searchStart)
        for (c <- complement) {
//          curSumRow(r) -= originalMatrix(sortedRows(r))(sortedCols(c))
          curSumRow(r) -= originalMatrix((r))(sortedCols(c))
        }
      }
    } else {
      for (r <- 0 until nRow) {
        curSumRow(r) = 0d
        for (c <- selected) {
//          curSumRow(r) += originalMatrix(sortedRows(r))(sortedCols(c))
          curSumRow(r) += originalMatrix((r))(sortedCols(c))
        }
      }
    }
    curSelRowNb = 0
    sumColPos(searchStart) = 0
    if(searchStart + 1 < nCol) {
      for(r <- 0 until nRow) {
        upperBound += Math.max(0, curSumRow(r) + cumSumRowRevertPos(r)(searchStart + 1))
        objectiveValue += Math.max(0, curSumRow(r) + cumSumRowRevert(r)(searchStart + 1))
        val a = curSumRow(r) + cumSumRowRevertPos(r)(searchStart)
        if(a > 0){
          curSelRow(curSelRowNb) = r
          curSelRowNb += 1
//          sumColPos(searchStart) += Math.max(0, originalMatrix(sortedRows(r))(sortedCols(searchStart)))
          sumColPos(searchStart) += Math.max(0, originalMatrix((r))(sortedCols(searchStart)))
        }
      }
    } else {
      for(r <- 0 until nRow) {
        upperBound += Math.max(0, curSumRow(r))
        objectiveValue += Math.max(0, curSumRow(r))
        if(curSumRow(r) > 0){
          curSelRow(curSelRowNb) = r
          curSelRowNb += 1
        }
      }
    }
    new Candidate(upperBound, objectiveValue, searchStart, selected)
  }

  def branchWithSwap(searchStart: Int, selected: Array[Int]): Candidate = {
    var upperBound: Double = 0d
    var objectiveValue: Double = 0d
    if(searchStart > 0) {
      sumColPos(searchStart) = 0
      var r = 0
      while(r < curSelRowNb) {
//        curSumRow(curSelRow(r)) += originalMatrix(curSelRow(sortedRows(r)))(sortedCols(searchStart - 1))
        curSumRow(curSelRow(r)) += originalMatrix(curSelRow((r)))(sortedCols(searchStart - 1))
        if(curSumRow(curSelRow(r)) + cumSumRowRevertPos(curSelRow(r))(searchStart) < 0){
          curSelRow(r) = curSelRow(curSelRowNb - 1)
          curSelRowNb -= 1
          r -= 1
        } else {
//          sumColPos(searchStart) += Math.max(0, originalMatrix(curSelRow(sortedRows(r)))(sortedCols(searchStart)))
          sumColPos(searchStart) += Math.max(0, originalMatrix(curSelRow((r)))(sortedCols(searchStart)))
        }
        r += 1
      }
    }
    if(searchStart + 1 < nCol) {
      for(r <- 0 until curSelRowNb){
        upperBound += Math.max(0, curSumRow(curSelRow(r)) + cumSumRowRevertPos(curSelRow(r))(searchStart + 1))
        objectiveValue += Math.max(0, curSumRow(curSelRow(r)) + cumSumRowRevert(curSelRow(r))(searchStart + 1))
      }
    } else {
      for(r <- 0 until curSelRowNb){
        upperBound += Math.max(0, curSumRow(curSelRow(r)))
        objectiveValue += Math.max(0, curSumRow(curSelRow(r)))
      }
    }
    new Candidate(upperBound, objectiveValue, searchStart, selected)
  }


  def bndAndObjInit(): Candidate = {
    var ub: Double = 0d
    var obj: Double = 0d
    for(r <- 0 until nRow){
      ub += Math.max(0, cumSumRowRevertPos(r)(0))
      obj += Math.max(0, cumSumRowRevert(r)(0))
    }
    new Candidate(ub, obj, -1, Array[Int]())
  }

  def updateBest(curCand: Candidate): Unit = {
    bestSol = curCand
//    TIMES.append(System.nanoTime() - time)
//    SCORES.append(bestSol.getOBJ())
    addSolution(bestSol.getSelectedCols())
    maxUpB = reduceHeapSize()
    println(toString())
  }

  def reduceHeapSize(): Double = {
    var mUP: Double = curSol.getUB()
    val prevQueueSize: Int = queue.size
    queue = queue.filter(isValidCandidate(_))
    if(!queue.isEmpty){
      mUP = queue.maxBy(c => c.getUB()).getUB()
    }
//    var newqueue: mutable.PriorityQueue[Candidate] = new mutable.PriorityQueue[Candidate]() //TODO: modified by replacing a filter and a loop on max
//    while(!queue.isEmpty){
//      var curCan: Candidate = queue.dequeue()
//      if(isValidCandidate(curCan)){
//        newqueue.enqueue(curCan)
//        if(curCan.getUB() > mUP){
//          mUP = curCan.getUB()
//        }
//      }
//    }
//    queue = newqueue
    nbPruned += (prevQueueSize - queue.size)
    mUP
  }

  def isValidCandidate(cand: Candidate): Boolean = {
    cand.getUB() > bestSol.getOBJ()
  }

  def store(cand: Candidate): Boolean = {
    if(isValidCandidate(cand) && cand.getUB() > cand.getOBJ()){
      queue.enqueue(cand)
      true
    } else {
      nbPruned += 1
      false
    }
  }

  def compareToIncumbent(cand: Candidate): Unit = {
    if(cand.getOBJ() > bestSol.getOBJ()){
      nbImprovements += 1
      updateBest(cand)
    }
  }





  override def toString(): String = {
    bestSol + "\tMax UB: " + maxUpB + "\tGap: " + "%.2f".format(((maxUpB-bestSol.getOBJ())/bestSol.getOBJ())*100d) + "\tTime (nanoseconds): " + java.text.NumberFormat.getIntegerInstance.format(((System.nanoTime()-time)).toInt)
  }

  class Candidate(private val upperBound: Double,
                  private val objectiveValue: Double,
                  private val searchPos: Int,
                  private val selectedCols: Array[Int]){

    private val key: Double = {
      upperBound * phi + (1d - phi) * objectiveValue
    }
    def getUB(): Double = upperBound
    def getOBJ(): Double = objectiveValue
    def getBNDED(): Int = searchPos
    def getSelectedCols(): Array[Int] = selectedCols

    override def toString(): String = objectiveValue + "\t[" + upperBound + "]"
  }
  object Candidate {
    implicit def oerderingByKey[A <: Candidate]: Ordering[A] = Ordering.by(c => c.key)
  }

}
