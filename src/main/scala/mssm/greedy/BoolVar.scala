package mssm.greedy

import oscar.algo.Inconsistency

/**
  * Created by pschaus on 31/03/17.
  */
class BoolVar {


  private final val FALSE = 0
  private final val TRUE = 1
  private final val UNASSIGNED = 2

  private[this] var value = UNASSIGNED

  def isTrue = value == TRUE

  def isFalse = value == FALSE

  def isBound = value != UNASSIGNED

  def assignTrue() {
    if (isFalse) throw Inconsistency
    else value = TRUE
  }

  def assignFalse(): Unit = {
    if (isTrue) throw Inconsistency
    else value = FALSE
  }



}
