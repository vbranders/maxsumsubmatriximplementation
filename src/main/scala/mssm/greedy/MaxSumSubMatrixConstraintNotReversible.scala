package mssm.greedy

import oscar.algo.Inconsistency
import oscar.algo.reversible.ReversibleInt

/**
  * Global Constraint also doing the branch and bound internally as at every level of decision
  * we have a feasible decision.
  */
class MaxSumSubMatrixConstraintNotReversible(x: Array[BoolVar], matrix: Array[Array[Double]],lightFilter: Boolean) {

  var onSolution: () => Unit = () => {}


  val n = matrix.size
  val m = matrix(0).size

  val partialSum = Array.tabulate(n)(i => 0.0)

  // Reversible sparse-set trick to maintain unbound variables
  var unBound = Array.tabulate(m)(i => i)
  var nUnBound = m

  // Reversible sparse-set trick to maintain possibly interesting lines
  var candidate = Array.tabulate(n)(i => i)
  var nCandidate = m



  var bestBound = Double.MinValue


  def removeBoundVariablesAndUpdatePartialSum(): Unit = {
    var size = nUnBound
    var j = size
    while (j > 0) {
      j -= 1
      if (x(unBound(j)).isBound) {
        size -= 1
        val tmp = unBound(j)
        if (x(tmp).isTrue) select(tmp)
        unBound(j) = unBound(size)
        unBound(size) = tmp
      }
    }
    nUnBound = size
  }

  def select(c: Int): Unit = {
    var i = nCandidate
    while (i > 0) {
      i -= 1
      partialSum(candidate(i)) += matrix(candidate(i))(c)
    }
  }

  def computeObjective(): Double = {
    var objective = 0.0
    var i = nCandidate
    while (i > 0) {
      i -= 1
      var pSum = partialSum(candidate(i))
      if (pSum > 0) {
        objective += pSum
      }
    }
    return objective
  }

  def pruneLight() = {
    var objectiveUB = 0.0
    var nCand = nCandidate
    var i = nCand
    while (i > 0) {
      i -= 1
      var costLineUB = partialSum(candidate(i))
      // optimistic selection of positive values only for the UB
      var j = nUnBound
      while (j > 0) {
        j -= 1
        if (matrix(candidate(i))(unBound(j)) > 0) {
          costLineUB += matrix(candidate(i))(unBound(j))
        }
      }
      if (costLineUB > 0) {
        objectiveUB += costLineUB
      } else {
        // remove this line as it can never become positive under this node
        nCand -= 1
        val tmp = candidate(i)
        candidate(i) = candidate(nCand)
        candidate(nCand) = tmp
      }
    }
    nCandidate = nCand
    // Pruning of the branch and bound
    if (objectiveUB <= bestBound) {
      throw Inconsistency
    }

  }

  var objectiveUBNotSelect = Array.fill[Double](m)(0.0)
  var objectiveUBForceSelect = Array.fill[Double](m)(0)


  def prune(): Unit = {

    var objectiveUB = 0.0
    val nCols = nUnBound
    var j: Int = nCols
    while (j > 0) {
      j -= 1
      objectiveUBNotSelect(unBound(j)) = 0
      objectiveUBForceSelect(unBound(j)) = 0
    }

    var nCand = nCandidate
    var i = nCand
    while (i > 0) {
      i -= 1
      var costLineUB = partialSum(candidate(i))
      // optimistic selection of positive values only for the UB
      j = nCols
      while (j > 0) {
        j -= 1
        if (matrix(candidate(i))(unBound(j)) > 0) {
          costLineUB += matrix(candidate(i))(unBound(j))
        }
      }
      if (costLineUB > 0) {
        objectiveUB += costLineUB

        // objectiveUBNotSelect(j) = upper bound if column j would not be selected
        j = nCols
        while (j > 0) {
          j -= 1
          if (matrix(candidate(i))(unBound(j)) > 0) {
            if (costLineUB - matrix(candidate(i))(unBound(j)) > 0) {
              objectiveUBNotSelect(unBound(j)) += costLineUB - matrix(candidate(i))(unBound(j))
            }
            objectiveUBForceSelect(unBound(j)) += costLineUB
          } else {
            objectiveUBNotSelect(unBound(j)) += costLineUB
            if (costLineUB + matrix(candidate(i))(unBound(j)) > 0) {
              objectiveUBForceSelect(unBound(j)) += costLineUB + matrix(candidate(i))(unBound(j))
            }
          }
        }

      } else {
        // remove this line as it can never become positive under this node
        nCand -= 1
        val tmp = candidate(i)
        candidate(i) = candidate(nCand)
        candidate(nCand) = tmp
      }

    }
    // Pruning of the branch and bound
    if (objectiveUB <= bestBound) {
      throw Inconsistency
    }
    j = nCols
    while (j > 0) {
      j -= 1
      if (objectiveUBNotSelect(unBound(j)) <= bestBound) {
        x(unBound(j)).assignTrue()
      }
      if (objectiveUBForceSelect(unBound(j)) <= bestBound) {
        x(unBound(j)).assignFalse()
      }
    }
    nCandidate = nCand
  }

  def propagate(): Unit = {
    removeBoundVariablesAndUpdatePartialSum()
    val objective = computeObjective()
    if (objective > bestBound) {
      bestBound = objective
      onSolution()
      //      println("new best bound:" + objective)
    }
    if (lightFilter) pruneLight()
    else prune()
  }



}
