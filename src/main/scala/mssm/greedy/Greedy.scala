package mssm.greedy

import mssm.SolutionFinder
import oscar.cp._
import oscar.algo.Inconsistency

import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
  * @author Pierre Schaus
  */
object Greedy extends SolutionFinder {

  /**
    * Arg:
    *   (0): file path to matrix
    *   (1): time budget in seconds
    *   (2): detail for the name (e.g. to store alternative experiments)
    *   (3): true to use lightFilter, false otherwise
    */

  private val filePath = args(0)
  private val timeMax = defaultOrArg(1, 10).toLong * 1000 * 1000 * 1000 // maximum allocated time in nanoseconds
  private val fileComplement = defaultOrArg(2, "")
  private val lightFilter = defaultOrArg(3, false)

  private val methodName = "Greedy" + (if(lightFilter) "_lightFilter" else "_fullFilter")
  private val methodDescr = {
    "Greedy" + (if(lightFilter) "with light filter" else " with full filter")
  }

  def getName(): String = methodName + fileComplement
  def getDescription(): String = methodDescr
  def getScoreBest(): Double = bestScore
  def getTimeBudget(): Long = timeMax
  def getFileName(): String = filePath


  // ---------------------------------------- Reading the matrix ----------------------------------------
  val originalMatrix = Source.fromFile(filePath).getLines().toArray.map(_.split(valueSeparator).map(_.toDouble))
  val nRow: Int = originalMatrix.size
  val nCol: Int = originalMatrix(0).size

  var bestScore: Double = Double.MinValue
  println("root UB:" + originalMatrix.map(_.filter(_ > 0).sum).sum)

  // -------------- CP Model --------------


  val x = Array.fill[BoolVar](nCol)(new BoolVar())

  val xBest = Array.ofDim[Int](nCol)


  val xsorted = {
    val matrixRevert = Array.tabulate(nCol)(j => Array.tabulate(nRow)(i => originalMatrix(i)(j)))
    val sumCol = Array.tabulate(nCol)(j => matrixRevert(j).filter(_ > 0).sum)
    val permSumCol = (0 until nCol).sortBy(j => -sumCol(j))
    Array.tabulate(nCol)(i => x(permSumCol(i)))
  }

  val maxSumConstraint = new MaxSumSubMatrixConstraintNotReversible(x, originalMatrix, lightFilter)

  printinit()
  private val time: Long = System.nanoTime()


  maxSumConstraint.onSolution = () => {
//    TIMES.append(System.nanoTime() - time)
//    SCORES.append(bestScore.toFloat)
    bestScore = maxSumConstraint.bestBound
    println("new best bound:" + bestScore + "\tTime (nanoseconds): " + java.text.NumberFormat.getIntegerInstance.format(((System.nanoTime()-time)).toInt))
    // record best solution
    for (i <- 0 until x.size) {
      if (x(i).isTrue) xBest(i) = 1
      else xBest(i) = 0
    }
    addSolution((0 until x.size).filter(i => xBest(i) == 1))
    // println("new best bound:" + maxSumConstraint.bestBound)
    //    val cTime: Long = (System.nanoTime()-time)/1000000000
    //    val h = (cTime/(3600)).toInt
    //    val m = ((cTime%3600)/60).toInt
    //    val s = (cTime%60).toInt
    //    println("new best bound:" + bestScore + "\tTime: " + h + "h, " + m + "m, " + s + "s")
  }

  try {
    for (i <- 0 until nCol; if !xsorted(i).isBound) {
      xsorted(i).assignTrue()
      maxSumConstraint.propagate()
    }
  } catch {
    case _ : Throwable =>
  }


  requiredTime = System.nanoTime()-time
  println(requiredTime)
  completed = true
  storeJSON()

}
