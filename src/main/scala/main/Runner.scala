package main

import java.io.File

import mssm_k_no_overlap.{ColGenK, MIP}
import mssm.SolutionFinder
import mssm.bb.BranchAndBound
import mssm.colGeneration.ColumnGeneration
import mssm.cp.{CPGC, CPGCk, CP_LNS}
import mssm.greedy.Greedy
import utils.{ArgsParser, FileSaver}

import scala.io.Source

/**
  * Created by vbranders on 19/04/17.
  *
  */
object Runner extends App {

  val cmd = Map(
    "-t" -> Array("Int", "10", "Budget limit in seconds"),
    "-v" -> Array("Int", "0", "Verbose (Yes:1|No:0)"),
    "-e" -> Array("Int", "1", "Exit on end (Yes:1|No:0)")
  )

  if(args.contains("-K")){
    Multiple.main(args)
  } else {

    println("Run in single search mode")

    val approach: String = args(0)
    val path: String = args(1)
    val parser = new ArgsParser()
    val res = parser.parse(cmd, args.drop(2))
    val timeMax: Long = res("-t").toLong
    val verbose: Boolean = res("-v") == "1"
    val exit: Boolean = res("-e") == "1"

    if (verbose) {
      println("------------------------------------------------------------------------------------------------------------------------")
      println("Approach: \t" + approach)
      println("File: \t" + path)
      println("Time max: \t" + java.text.NumberFormat.getIntegerInstance.format(timeMax) + "\n")
    }

    //Defines the approach to use
    val appToRun: SolutionFinder = approach match {
      case "BB" => BranchAndBound
      case "CPGC" => CPGC
      case "CPGCk" => CPGCk
      case "CP" => CP_LNS
      case "Greedy" => Greedy
      case "CG" => ColumnGeneration
      case "CGK" => ColGenK
      case "MIPNO" => MIP
      case _ => {
        println("Error !\nApproach not found (" + approach + ")"); System.exit(0);
        BranchAndBound
      }
    }

    //Prepare thread to run the approach
    var fail: Boolean = false
    var thread = new Thread {
      override def run(): Unit = {
        try {
          appToRun.main(args.drop(1))
        } catch {
          case e: Exception => {
            fail = true;
            appToRun.setFailed(true); appToRun.storeJSON(); println(e.getMessage); println(e.getStackTrace.mkString("\n"))
          }
        }
      }
    }
    //Start the thread
    thread.start()
    //With limited time
    thread.join(timeMax * 1000)
    //Test if thread is still alive
    val completed = !thread.isAlive
    appToRun.setFailed(fail)
    appToRun.storeJSON()
    if (verbose) {
      println(if (completed) "Completed" else "Interrupted")
      println(if (fail) "failed")
    }
    //Force interruption if required
    if(thread.isAlive) {
      appToRun.forceInterrupt()
    }

    var uselesscnt = 0
    while(thread != null && thread.isAlive){
      uselesscnt += 1
      thread.interrupt()
      thread = null
    }


    if (verbose) {
      println("------------------------------------------------------------------------------------------------------------------------")
    }

    if(exit) {
      System.exit(0)
    }

  }

}
