package main

import java.io.File

import mssm.bb.BranchAndBound
import mssm.colGeneration.ColumnGeneration
import mssm.cp.{CPGC, CPGCk, CP_LNS}
import mssm.greedy.Greedy
import utils.{ArgsParser, FileSaver}

import scala.io.Source

/**
  * Created by vbranders on 11/09/18.
  *
  */
object Multiple extends App {

  val cmd = Map(
    "-t" -> Array("Int", "10", "Budget limit in seconds"),
    "-v" -> Array("Int", "0", "Verbose (Yes:1|No:0)"),
    "-e" -> Array("Int", "1", "Exit on end (Yes:1|No:0)"),
    "-K" -> Array("Int", "1", "Number of submatrices to find")
  )

  val approach: String = args(0)
  val path: String = args(1).substring(0, args(1).lastIndexOf('/')+1)
  val matrixName = args(1).substring(args(1).lastIndexOf('/')+1)
  val parser = new ArgsParser()
  val res = parser.parse(cmd, args.drop(2))
  val timeMax: Long = res("-t").toLong
  val verbose: Boolean = res("-v") == "1"
  val exit: Boolean = res("-e") == "1"

  val params = args.drop(1) //Remove first arg
  val nIter: Int = res("-K").toInt

  //Load the original matrix
  var originalMatrix: Array[Array[Double]] = Source.fromFile(path + matrixName).getLines().toArray.map(_.split("\t").map(_.toDouble))

  //Store as matrix 1.txt (to be removed)
  new FileSaver(originalMatrix.map(_.mkString("\t")).mkString("\n"), path + (1).toString + ".txt")

  println("Running on " + 1 + " until " + (nIter+1))
  for(k <- 1 until (nIter+1)){
    params.update(0, path + k + ".txt") //Replace the first argument, path to the matrix to use, by the newly created matrix

    //Ensure that files with answer does not exists
    new File(path + k + "_rows.txt").delete()
    new File(path + k + "_cols.txt").delete()

    if (verbose) {
      println("------------------------------------------------------------------------------------------------------------------------")
      println("Approach: \t" + approach)
      println("File: \t" + params(0))
      println("Time max: \t" + java.text.NumberFormat.getIntegerInstance.format(timeMax) + "\n")
    }

    //Defines the approach to use
    val appToRun = approach match {
      case "BB" => BranchAndBound
      case "CPGC" => CPGC
      case "CPGCk" => CPGCk
      case "CP" => CP_LNS
      case "Greedy" => Greedy
      case "CG" => ColumnGeneration
      case _ => {
        println("Error !\nApproach not found (" + approach + ")"); System.exit(0);
        BranchAndBound
      }
    }

    //Prepare thread to run the approach
    var fail: Boolean = false
    var thread = new Thread {
      override def run(): Unit = {
        try {
          appToRun.main(params)
        } catch {
          case e: Exception => {
            fail = true;
            appToRun.setFailed(true); appToRun.storeJSON(); println(e.getMessage); println(e.getStackTrace.mkString("\n"))
          }
        }
      }
    }
    //Start the thread
    thread.start()
    //With limited time
    thread.join(timeMax * 1000)
    //Test if thread is still alive
    val completed = !thread.isAlive
    appToRun.setFailed(fail)
    appToRun.storeJSON()
    if (verbose) {
      println(if (completed) "Completed" else "Interrupted")
      println(if (fail) "failed")
    }
    //Force interruption if required
    if(thread.isAlive) {
      appToRun.forceInterrupt()
    }

    var uselesscnt = 0
    while(thread != null && thread.isAlive){
      uselesscnt += 1
      thread.interrupt()
      thread = null
    }


    if (verbose) {
      println("------------------------------------------------------------------------------------------------------------------------")
    }
    if(exit && fail) {
      System.exit(0)
    }

    //Load the original matrix
    var originalMatrix: Array[Array[Double]] = Source.fromFile(path + k + ".txt").getLines().toArray.map(_.split("\t").map(_.toDouble))

    //Wait for the results of the search to be created in files
    var cntUseless = 0;
    while(!new java.io.File(path + k + "_rows.txt").exists) {
      cntUseless += 1
    }

    //Replace the selected entries by null values in the matrix
    val rows: Array[Int] = Source.fromFile(path + k + "_rows.txt").getLines().next().split(" ").map(i => (i.toInt) - 1)
    val cols: Array[Int] = Source.fromFile(path + k + "_cols.txt").getLines().next().split(" ").map(i => (i.toInt) - 1)
    for(r <- rows){
      for(c <- cols){
        originalMatrix(r)(c) = 0
      }
    }
    //Store the associated masked matrix
    new FileSaver(originalMatrix.map(_.mkString("\t")).mkString("\n"), path + (k+1).toString + ".txt")
    //Remove the previous matrix
    new File(path + (k).toString + ".txt").delete
    //Wait for the new matrix to be created
    cntUseless = 0
    while(!new File(path + (k+1).toString + ".txt").exists) {
      cntUseless += 1
    }
  }
  //Remove the last created matrix
  new File(path + (nIter+1).toString + ".txt").delete

  if(exit) {
    System.exit(0)
  }
}
