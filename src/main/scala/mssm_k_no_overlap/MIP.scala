package mssm_k_no_overlap

import main.Runner
import gurobi._
import mssm.SolutionFinder
import utils.{ArgsParser, FileSaver}

import scala.collection.mutable.ArrayBuffer
import scala.io.Source


/**
  * Created by vbranders on 13/11/17.
  *
  */
object MIP extends App with SolutionFinder {

  /**
    * Arg:
    *   (0): file path to matrix
    *   (1): time budget in seconds
    *   (2): detail for the name (e.g. to store alternative experiments)
    */

  val cmd = Map(
    "-t" -> Array("Int", "10", "Budget limit in seconds"),
    "-n" -> Array("String", "", "Name of experiment (included in stored files)"),
    "-v" -> Array("Int", "0", "Verbose (Yes:1|No:0)")
  )

  val parser = new ArgsParser()
  val argsParsed = parser.parse(cmd, args.drop(1))

  private val filePath = args(0)
  private val timeMax = argsParsed("-t").toLong * 1000 * 1000 * 1000 // maximum allocated time in nanoseconds
  private val fileComplement = argsParsed("-n")//defaultOrArg(2, "")
  verbose = argsParsed("-v") == "1"


  private val methodName = "MIPNO"
  private val methodDescr = "MIP NO"

  def getName(): String = methodName + fileComplement
  def getDescription(): String = methodDescr
  def getScoreBest(): Double = bestScore
  def getTimeBudget(): Long = timeMax
  def getFileName(): String = filePath

  val path_log = filePath + ".mip.log"

  // ---------------------------------------- Reading the matrix ----------------------------------------
  // Load original matrix
  val originalMatrix = Source.fromFile(filePath).getLines().toArray.map(_.split(valueSeparator).map(_.toDouble))
  // Load similar matrix
  val copyMatrix = Source.fromFile(filePath).getLines().toArray.map(_.split(valueSeparator).map(_.toDouble))
  val nRow: Int = originalMatrix.size
  val nCol: Int = originalMatrix(0).size

  // - MIP Model
  var bestScore: Double = Double.MinValue
  val env: GRBEnv = new GRBEnv(path_log)
  val model: GRBModel = new GRBModel(env)
  env.set(GRB.IntParam.LogToConsole, 0)
  model.set(GRB.IntParam.Method, 2)
  //-1=automatic, 0=primal simplex, 1=dual simplex, 2=barrier, 3=concurrent, 4=deterministic concurrent, 5=deterministic concurrent simplex.

  val objective = new GRBLinExpr()

  val K = 2

  //lb, ub, obj, vtype, name
  val T: Array[Array[GRBVar]] = Array.tabulate(nRow)(i => Array.tabulate(K)(k => model.addVar(0, 1, 0, GRB.BINARY, "T_"+i+"^"+k)))
  val U: Array[Array[GRBVar]] = Array.tabulate(nCol)(j => Array.tabulate(K)(k => model.addVar(0, 1, 0, GRB.BINARY, "U_"+j+"^"+k)))
  val vProduct: Array[Array[Array[GRBVar]]] = Array.ofDim(nRow, nCol, K)
  val r: Array[Array[GRBLinExpr]] = Array.ofDim(nRow, K)
  val rPositive: Array[Array[GRBVar]] = Array.ofDim(nRow, K)
  var MPos: Array[Double] = Array.tabulate(nRow)(i => 0.0)
  var MNeg: Array[Double] = Array.tabulate(nRow)(i => 0.0)

  for(k <- 0 until K){
    for(i <- 0 until nRow){
      r(i)(k)= new GRBLinExpr()
      for(j <- 0 until nCol){
        r(i)(k).addTerm(originalMatrix(i)(j), U(j)(k))

        // Add constraint that defines v_ij^k
        vProduct(i)(j)(k) = model.addVar(0, 1, 0, GRB.BINARY, "v_{"+i+","+j+"}^"+k)
        val lTerm = new GRBLinExpr()
        lTerm.addTerm(2, vProduct(i)(j)(k))
        val cTerm = new GRBLinExpr()
        cTerm.addTerm(1.0, T(i)(k))
        cTerm.addTerm(1.0, U(j)(k))
        model.addConstr(lTerm, GRB.LESS_EQUAL, cTerm, "2*v_i_j_k <= T_i_k + U_j_k - "+ i + " - " + j + " - " + k)
        val rTerm = new GRBLinExpr()
        rTerm.addConstant(1.0)
        rTerm.addTerm(1.0, vProduct(i)(j)(k))
        model.addConstr(cTerm, GRB.LESS_EQUAL, rTerm, "T_i_k + U_j_k <= 1 + v_i_j_k - "+ i + " - " + j + " - " + k)
      }
    }
  }
  for(i <- 0 until nRow){
    for(j <- 0 until nCol){
      MPos(i) += math.max(0, originalMatrix(i)(j))
      MNeg(i) += math.min(0, originalMatrix(i)(j))

      // Add constraint
      val term = new GRBLinExpr()
      for(k <- 0 until K) {
        term.addTerm(1.0, vProduct(i)(j)(k))
      }
      model.addConstr(term, GRB.LESS_EQUAL, 1.0, "sum T_i_k * U_j_k <= 1 - "+i+" - "+j)
    }
    for(k <- 0 until K) {
      rPositive(i)(k) = model.addVar(0.0, MPos(i), 0.0, GRB.CONTINUOUS, "r_"+i+"^"+k+"+")

      // add constraint r_i^k+ <= T_i^k * Mpos
      val term = new GRBLinExpr()
      term.addTerm(MPos(i), T(i)(k))
      model.addConstr(rPositive(i)(k), GRB.LESS_EQUAL, term, "r_"+i+"^"+k+"+ <= T_"+i+"^"+k+" * Mpos")

      // add constraint r_i^+ <= r_i^k + Mneg - Mneg*T_i^
      val term2 = new GRBLinExpr()
      term2.add(r(i)(k))
      term2.addConstant(MPos(i))
      term2.addTerm(-MPos(i), T(i)(k))
      model.addConstr(rPositive(i)(k), GRB.LESS_EQUAL, term2, "r_"+i+"^"+k+"+ <= r_"+i+"^"+k+" + Mneg - Mneg * T_"+i+"^"+k)

      // defines objective
      objective.addTerm(1.0, rPositive(i)(k))
    }
  }

  printinit()
  private val startTime: Long = System.currentTimeMillis()

  model.setObjective(objective, GRB.MAXIMIZE)

  val callback = new StoreProgression

  model.setCallback(callback)

  model.set(GRB.IntParam.Presolve, 0)
  model.set(GRB.IntParam.Threads, 1)

  // Optimize model
  model.optimize()
  // Compute best score
  bestScore = model.get(GRB.DoubleAttr.ObjVal).toFloat
  // Print the objective value
  println("Obj: " + model.get(GRB.DoubleAttr.ObjVal))

  println("--- COLS ---")
  println("\t\t" + Array.tabulate(K)(k => Array.tabulate(nCol)(j => if(U(j)(k).get(GRB.DoubleAttr.X) > 0){U(j)(k).get(GRB.DoubleAttr.X)}else{"   "}).mkString(" | ")).mkString("\n\t\t"))
  println("--- ROWS ---")
  println(Array.tabulate(nRow)(i => Array.tabulate(K)(k => if(T(i)(k).get(GRB.DoubleAttr.X) > 0){T(i)(k).get(GRB.DoubleAttr.X)}else{"   "}).mkString(" | ")).mkString("\n"))
  println("--- ---")
  println(Array.tabulate(nRow)(i => Array.tabulate(K)(k => rPositive(i)(k).get(GRB.DoubleAttr.X)).mkString(" | ")).mkString("\n"))

  // Dispose of model and environment
  model.dispose
  env.dispose

  println("Terminated")
  requiredTime = System.currentTimeMillis() - startTime
  completed = true
  failed = false

  class StoreProgression extends GRBCallback {

    def callback() = {
      if(where == GRB.CB_MIPSOL){
        val obj: Double = getDoubleInfo(GRB.CB_MIPSOL_OBJ)


        bestScore = obj.toFloat
      }
    }

  }

}
