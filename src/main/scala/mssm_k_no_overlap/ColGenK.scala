package mssm_k_no_overlap

import main.Runner
import mssm.SolutionFinder
import utils.{ArgsParser, FileSaver}

import scala.collection.mutable.ArrayBuffer
import scala.io.Source


/**
  * Created by vbranders on 13/11/17.
  *
  */
object ColGenK extends App with SolutionFinder {

  /**
    * Arg:
    *   (0): file path to matrix
    *   (1): time budget in seconds
    *   (2): detail for the name (e.g. to store alternative experiments)
    */

  val cmd = Map(
    "-t" -> Array("Int", "10", "Budget limit in seconds"),
    "-n" -> Array("String", "", "Name of experiment (included in stored files)"),
    "-v" -> Array("Int", "0", "Verbose (Yes:1|No:0)")
  )

  val parser = new ArgsParser()
  val argsParsed = parser.parse(cmd, args.drop(1))

  private val filePath = args(0)
  private val timeMax = argsParsed("-t").toLong * 1000 * 1000 * 1000 // maximum allocated time in nanoseconds
  private val fileComplement = argsParsed("-n")//defaultOrArg(2, "")
  verbose = argsParsed("-v") == "1"


  private val methodName = "GC"
  private val methodDescr = "Column generation"

  def getName(): String = methodName + fileComplement
  def getDescription(): String = methodDescr
  def getScoreBest(): Double = bestScore
  def getTimeBudget(): Long = timeMax
  def getFileName(): String = filePath


//  val path_T = filePath+".col_T"
//  val path_U = filePath+".col_U"
  val path_log = filePath + ".mip.log"
  val path_subproblem = filePath + "_subproblem_"

  val x_var_lb = 0
  val x_var_ub = 1
  val x_var_obj = 0.0
  val x_var_vtype = GRB.BINARY //GRB.CONTINUOUS
  val x_var_name = "x_"


  // ---------------------------------------- Reading the matrix ----------------------------------------
  // Load original matrix
  val originalMatrix = Source.fromFile(filePath).getLines().toArray.map(_.split(valueSeparator).map(_.toDouble))
  // Load similar matrix
  val copyMatrix = Source.fromFile(filePath).getLines().toArray.map(_.split(valueSeparator).map(_.toDouble))
  val nRow: Int = originalMatrix.size
  val nCol: Int = originalMatrix(0).size

  // Fill boolean rows and columns vector with empty selection
//  new FileSaver("", path_T, false)
//  new FileSaver(Array.tabulate(nRow)(i => 0).mkString(valueSeparator)+"\n", path_T, true)
//  new FileSaver("", path_U, false)
//  new FileSaver(Array.tabulate(nCol)(i => 0).mkString(valueSeparator)+"\n", path_U, true)
//
//  // Load rows and columns represented as two boolean vectors
//  val T = Source.fromFile(path_T).getLines().toArray.map(_.split(valueSeparator).map(_.toInt)).toBuffer
//  val U = Source.fromFile(path_U).getLines().toArray.map(_.split(valueSeparator).map(_.toInt)).toBuffer

  // Array of submatrices, each entry is an array of the row boolean vector and of the column boolean vector
  val columns: ArrayBuffer[Array[Array[Int]]] = new ArrayBuffer[Array[Array[Int]]]()
  // Defines the list of rows defining each submatrix
//  val solutionColumns_T: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()
//  // Defines the list of columns defininf each submatrix
//  val solutionColumns_U: ArrayBuffer[Array[Int]] = new ArrayBuffer[Array[Int]]()

  // Fills the rows and columns defining each submatrix with the new empty submatrix
//  solutionColumns_T += importSol(nRow, T)
  columns += Array(Array.fill(nRow)(0), Array.fill(nCol)(0))



  // - MIP Model
  var bestScore: Double = Double.MinValue

  val env: GRBEnv = new GRBEnv(path_log)
  val model: GRBModel = new GRBModel(env)
  env.set(GRB.IntParam.LogToConsole, 0)
  model.set(GRB.IntParam.Method, 2)
  //-1=automatic, 0=primal simplex, 1=dual simplex, 2=barrier, 3=concurrent, 4=deterministic concurrent, 5=deterministic concurrent simplex.
  val nSols = 100000

  val objective = new GRBLinExpr()

  val res: ArrayBuffer[Double] = new ArrayBuffer[Double]() //values of objective
  val K = 3
  val G = 1
  val x: ArrayBuffer[GRBVar] = ArrayBuffer.tabulate(columns.length)(k => model.addVar(x_var_lb, x_var_ub, x_var_obj, x_var_vtype, x_var_name+k.toString)) //x, selection of columns
  val cost: ArrayBuffer[Double] = new ArrayBuffer[Double]() //c, cost of a submatrix
  val sum_x_k: GRBLinExpr = new GRBLinExpr() //Sum of x_k which should be smaller or equal to K
  val coverage: Array[Array[Int]] = Array.ofDim(nRow, nCol)
  //Compute the cost for each column
  for(k <- 0 until columns.length){
    cost += 0.0
    for(r <- 0 until nRow if columns(k)(0)(r) > 0){
      for(c <- 0 until nCol if columns(k)(1)(c) > 0){
        cost(k) += originalMatrix(r)(c) * columns(k)(0)(r) * columns(k)(1)(c)
        coverage(r)(c) += columns(k)(0)(r) * columns(k)(1)(c) //Add the coverage to the matrix entry
      }
    }
    objective.addTerm(cost(k), x(k)) //Add the column to the objective
    sum_x_k.addTerm(1.0, x(k)) //Add the column to the dual variable
  }


  var z_constraint: GRBConstr = model.addConstr(sum_x_k, GRB.LESS_EQUAL, K, "Z")
  var b_constraint: ArrayBuffer[GRBConstr] = new ArrayBuffer[GRBConstr]()
  for(i <- 0 until columns.length){
    b_constraint += model.addConstr(x(i), GRB.LESS_EQUAL, 1, "B_"+i)
  }

  var coverage_constraint: Array[Array[GRBConstr]] = Array.ofDim(nRow, nCol)
  val Y: Array[Array[GRBLinExpr]] = Array.ofDim(nRow, nCol) //Dual variable Y

  for(r <- 0 until nRow){
    for(c <- 0 until nCol){
      if(coverage(r)(c) > G) { //If constraint must be applied on it
        val term = new GRBLinExpr()
        for(k <- 0 until columns.length){
          term.addTerm(columns(k)(0)(r)*columns(k)(1)(c), x(k))
        }
        Y(r)(c) = term
        coverage_constraint(r)(c) = model.addConstr(Y(r)(c), GRB.LESS_EQUAL, G, "Y_{"+r+","+c+"}")
      }
      //ELSE Y(r)(c) = 0
    }
  }

  printinit()
  private val startTime: Long = System.currentTimeMillis()

  model.setObjective(objective, GRB.MAXIMIZE)

  val callback = new StoreProgression

  model.setCallback(callback)

  model.set(GRB.IntParam.Presolve, 0)
  model.set(GRB.IntParam.Threads, 1)

  model.update()

  var do_search = true
  var n_c = columns.length
  //Perform a search
  while(do_search){
    n_c += 1
    // Solve the primal
    val relax = model.relax()
    relax.getEnv.set(GRB.IntParam.LogToConsole, 0)
    relax.optimize()
    println("#Cols: " + x.length + "\tObj: " + relax.get(GRB.DoubleAttr.ObjVal))
    res += relax.get(GRB.DoubleAttr.ObjVal)

    // Primal solved

    // Prepare the subproblem
    for(r <- 0 until nRow){
      for(c <- 0 until nCol){
        if(coverage(r)(c) > G) {
          copyMatrix(r)(c) -= relax.getConstrByName("Y_{" + r + "," + c + "}").get(GRB.DoubleAttr.Pi)
        }
      }
    }
    // Subproblem prepared
    val path_subproblem_k = path_subproblem + n_c + ".tsv"
    //Store the associated matrix
    new FileSaver(copyMatrix.map(_.mkString(valueSeparator)).mkString("\n"), path_subproblem_k)

    val MINVAL = (relax.getConstrByName("Z").get(GRB.DoubleAttr.Pi) + ((0 until columns.length).map(i => relax.getConstrByName("B_"+i).get(GRB.DoubleAttr.Pi))).sum)
    var argsCPGC = ArrayBuffer("CPGC", path_subproblem_k) ++ args.drop(2)
    argsCPGC += ("-t", "120")
    argsCPGC += ("-target", MINVAL.toString)
    argsCPGC += ("-ns", nSols.toString)
    argsCPGC += ("-e", "0")
    argsCPGC += ("-c", "1")

    // Solve subproblem
    Runner.main(argsCPGC.toArray) //Solve the problem and build path_subproblem_k+".cols.tsv"


    // Get the new variable to be added to the primal
    columns += {
      val tmp1 = Source.fromFile(path_subproblem_k+".cols.tsv.T").getLines().toArray.map(_.split(valueSeparator).map(_.toInt))
      val tmp2 = Source.fromFile(path_subproblem_k+".cols.tsv.U").getLines().toArray.map(_.split(valueSeparator).map(_.toInt))
      Array(tmp1(0), tmp2(0))
    }

    // Compute solution to the subproblem
    var subprobSolution = 0.0
    for(r <- 0 until nRow){
      for(c <- 0 until nCol) {
        subprobSolution += copyMatrix(r)(c) * columns(n_c - 1)(0)(r) * columns(n_c - 1)(1)(c)
      }
    }

    // Restore original matrix
    for(r <- 0 until nRow){
      for(c <- 0 until nCol){
        if(coverage(r)(c) > G) {
          copyMatrix(r)(c) += relax.getConstrByName("Y_{" + r + "," + c + "}").get(GRB.DoubleAttr.Pi)
        }
      }
    }

    // If best submatrix is smaller than dual variable Z, than stop
    if(subprobSolution <= MINVAL  + 1e-8){
      do_search = false
      println("Stop Column Generation : proof of optimality !")
      println("Expecting: " + (MINVAL))
      println("Solution found: " + subprobSolution)
    }
    println("-------------------------------------------> Adding a column")

    // If we can continue
    //Create a new column
    val newColumn = new GRBColumn()
    var newColumnCost = 0.0
    for(r <- 0 until nRow){
      for(c <- 0 until nCol){
        // Add also to the coverage check
        coverage(r)(c) += columns(n_c-1)(0)(r)*columns(n_c-1)(1)(c)
        if(coverage(r)(c) > G) { //If constraint must be applied on it
          if(coverage(r)(c) == G + columns(n_c-1)(0)(r)*columns(n_c-1)(1)(c)) {
            Y(r)(c) = new GRBLinExpr()
            for (k <- 0 until columns.length - 1) {
              Y(r)(c).addTerm(columns(k)(0)(r) * columns(k)(1)(c), x(k))
            }
//            Y(r)(c) = term
            coverage_constraint(r)(c) = model.addConstr(Y(r)(c), GRB.LESS_EQUAL, G, "Y_{" + r + "," + c + "}")
          }
          // Add the column to the coverage constraint
          newColumn.addTerm(columns(n_c-1)(0)(r)*columns(n_c-1)(1)(c), coverage_constraint(r)(c))
        }
        // Compute cost of the column
        newColumnCost += originalMatrix(r)(c) * columns(n_c-1)(0)(r)*columns(n_c-1)(1)(c)
      }
    }
    // Store cost of column
    cost += newColumnCost
    // Add the column to the constraint Z testing for the number of submatrices
    newColumn.addTerm(1.0, z_constraint)
    // Add the column as a new variable x_k
    x += model.addVar(x_var_lb, x_var_ub, cost(n_c-1), x_var_vtype, newColumn, x_var_name+(n_c-1))
    b_constraint += model.addConstr(x(n_c-1), GRB.LESS_EQUAL, 1.0, "B_"+(n_c-1))
//    for(r <- 0 until nRow) {
//      for (c <- 0 until nCol) {
//        if (coverage(r)(c) == G + columns(n_c - 1)(0)(r) * columns(n_c - 1)(1)(c)) {
//          Y(r)(c).addTerm(columns(n_c)(0)(r) * columns(n_c)(1)(c), x(n_c))
//        }
//      }
//    }
    // Updates the model with the modifications
    model.update()
    println("New column added !")


    println("")

  }


  env.set(GRB.IntParam.LogToConsole, 0)

  // Optimize model
  model.optimize()
  // Compute best score
  bestScore = model.get(GRB.DoubleAttr.ObjVal).toFloat
  // Print the objective value
  println("Obj: " + model.get(GRB.DoubleAttr.ObjVal))
  // Compute the required time
  requiredTime = System.currentTimeMillis() - startTime
  // Print the time
  println("Comp. time: " + requiredTime)
  // Store the new objective value
  res += model.get(GRB.DoubleAttr.ObjVal)

  // Print command to plot the evolution in R
  println("x = c("+(1 to res.length).mkString(",")+")")
  println("y = c("+res.mkString(",")+")\nplot(x, y)")

  // Print all columns selected
  println("->>")
  println((0 until columns.length).filter(i => x(i).get(GRB.DoubleAttr.X) < 1000).map(i => x(i).get(GRB.DoubleAttr.X) + "\tCost: " + cost(i) + "\tRows: " + importSol(nRow, columns(i)(0)).mkString(",") + "\tCols: " + importSol(nCol, columns(i)(1)).mkString(",")).mkString("\n"))
  println("->>")
  println((0 until columns.length).filter(i => x(i).get(GRB.DoubleAttr.X) != 0.0).map(i => x(i).get(GRB.DoubleAttr.X) + "\tCost: " + cost(i) + "\tRows: " + importSol(nRow, columns(i)(0)).mkString(",") + "\tCols: " + importSol(nCol, columns(i)(1)).mkString(",")).mkString("\n"))
  println()

  println(model.getConstrs().length)


  // Dispose of model and environment
  model.dispose
  env.dispose

  println("Terminated")
  requiredTime = System.currentTimeMillis() - startTime
  completed = true
  failed = false









//  def computeCost(T, U):
//
//
//  /** Start by creating a file with an empty submatrix **/
//  getCols(nRow, nCol, filePath+".col") //Make empty file of columns
//  /** Loads the first submatrix represented as a boolean vector **/
//  val a = Source.fromFile(filePath+".col").getLines().toArray.map(_.split(valueSeparator).map(_.toInt)).toBuffer
//  /** Defines the list of submatrices represented as boolean vectors **/
//  val colToSol: ArrayBuffer[Array[Array[Int]]] = new ArrayBuffer[Array[Array[Int]]]()
//  colToSol += getSolI(nRow, nCol, a(0))  //Start with a first column full of ones
//  println("File read")
//
//
//
//  // ---------------------------------------- MIP Model ----------------------------------------
//  var bestScore: Double = Double.MinValue
//
//  val env: GRBEnv = new GRBEnv(filePath+".mip.log")
//  val model: GRBModel = new GRBModel(env)
//  env.set(GRB.IntParam.LogToConsole, 0)
//  model.set(GRB.IntParam.Method, 2)
//  //-1=automatic, 0=primal simplex, 1=dual simplex, 2=barrier, 3=concurrent, 4=deterministic concurrent, 5=deterministic concurrent simplex.
//  val nSols = 100000
//
//  val objective = new GRBLinExpr()
//
//  val res: ArrayBuffer[Double] = new ArrayBuffer()
//  val n = a.length //La taille de la matrice
//  val G = 1
//  val K = 3
//  val m = nRow*nCol
//  val x: ArrayBuffer[GRBVar] = ArrayBuffer.tabulate(a.length)(i => model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "x_"+i))
//  val c: ArrayBuffer[Double] = new ArrayBuffer()
//  val z: GRBLinExpr = new GRBLinExpr()
////  val cols: Array[GRBColumn] = Array.ofDim(1+(m*n))
//  for(i <- 0 until n){
//    c += 0.0
//    for(j <- 0 until m){
//      c(i) += originalMatrix(j/nCol)(j%nCol) * a(i)(j)
//    }
//    //    println(c(i))
//    objective.addTerm(c(i), x(i))
//    z.addTerm(1.0, x(i))
//  }
//  var sel: Array[GRBConstr] = Array.ofDim(m+1)
//  sel(m) = model.addConstr(z, GRB.LESS_EQUAL, K, "z")
//  val y: Array[GRBLinExpr] = Array.ofDim(m)
//  for(j <- 0 until m){
//    val term = new GRBLinExpr()
//    for(i <- 0 until n){
//      term.addTerm(a(i)(j), x(i))
//    }
//    y(j) = term
//    sel(j) = model.addConstr(y(j), GRB.LESS_EQUAL, G, "y_"+j)
//  }
//
////  val n = a.length
////  val G = 2
////  val K = 3.0
////  val m = nRow*nCol
////  //val a: Array[Array[GRBVar]] = Array.tabulate(n)(i => Array.tabulate(m)(j => model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "a_"+j+"_"+i)))
////  val x: Array[GRBVar] = Array.tabulate(n)(i => model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, "x_"+i))
////  val c: Array[Double] = Array.ofDim(n)
////  val z: GRBLinExpr = new GRBLinExpr()
////  for(i <- 0 until n){
////    c(i) = 0.0
////    for(j <- 0 until m){
////      c(i) += originalMatrix(j/nCol)(j%nCol) * a(i)(j)
////    }
////    //    println(c(i))
////    objective.addTerm(c(i), x(i))
////    z.addTerm(1.0, x(i))
////  }
////  model.addConstr(z, GRB.LESS_EQUAL, K, "z")
////  val y: Array[GRBLinExpr] = Array.ofDim(m)
////  for(j <- 0 until m){
////    val term = new GRBLinExpr()
////    for(i <- 0 until n){
////      term.addTerm(a(i)(j), x(i))
////    }
////    y(j) = term
////    model.addConstr(y(j), GRB.LESS_EQUAL, G, "y_"+j)
////  }
//
//
//  printinit()
//  private val time: Long = System.nanoTime()
//
//  model.setObjective(objective, GRB.MAXIMIZE)
//
//
//
//  val callback = new StoreProgression
//
//
//  model.setCallback(callback)
//
//  model.set(GRB.IntParam.Presolve, 0)
//  //model.set(GRB.IntParam.Cuts, 0)
//  model.set(GRB.IntParam.Threads, 1)
//
//  model.update()
//
//  var go = true
//  var nn = n
//  while(go){
//    nn = nn+1
////  for(nn <- (n+1) until a.length){
//    val relax = model.relax()
//    relax.getEnv.set(GRB.IntParam.LogToConsole, 0)
//    relax.optimize()
//    println("#Cols: " + x.length + "\tObj: " + relax.get(GRB.DoubleAttr.ObjVal))
//    res += relax.get(GRB.DoubleAttr.ObjVal)
//
////    var duals: Array[Double] = relax.getConstrs().map(x => x.get(GRB.DoubleAttr.Pi))
////    var duals = (0 until sel.length).map(x => relax.getConstr(x).get(GRB.DoubleAttr.Pi))//sel.map(x => x.get(GRB.DoubleAttr.Pi))//(0 until sel.length).map(x => relax.getRow(model.getConstr(0)).get.get(GRB.DoubleAttr.Pi))//Keep dual variables TODO
//    //var duals = relax.getConstrs().map(x => x.get(GRB.DoubleAttr.Pi))
////    println(">" + duals.size)
////    val sub_problem: GRBModel = new GRBModel(new GRBEnv(filePath+".mip-sub.log"))//Make the subproblem
////    sub_problem.
////    println("Dual value of z: " + relax.getConstrByName("z").get(GRB.DoubleAttr.Pi))
////    println("Dual value of z: " + duals(m))
//    val matrix = Source.fromFile(filePath).getLines().toArray.map(_.split(valueSeparator).map(_.toDouble))
//    for(i <- 0 until nRow){
//      for(j <- 0 until nCol){
//        matrix(i)(j) -= relax.getConstrByName("y_"+(i*nCol + j)).get(GRB.DoubleAttr.Pi)
////        matrix(i)(j) -= duals(i*nCol + j)
//      }
//    }
//    new FileSaver(matrix.map(_.mkString(valueSeparator)).mkString("\n"), filePath + "_subproblem.tsv")
//    //println(matrix.map(_.mkString(valueSeparator)).mkString("\t"))
//    var argsCPGC = ArrayBuffer("CPGC", filePath+"_subproblem.tsv") ++ args.drop(2)
//    argsCPGC += ("-t", "10")
//    argsCPGC += ("-lb", relax.getConstrByName("z").get(GRB.DoubleAttr.Pi).toString)
//    argsCPGC += ("-ns", nSols.toString)
//    argsCPGC += ("-e", "0")
//    argsCPGC += ("-c", "1")
//    Runner.main(argsCPGC.toArray) //Solve the problem and build filepath+"_subproblem.cols.tsv"
//    val na = Source.fromFile(filePath+"_subproblem.tsv"+"_subproblem.cols.tsv").getLines().toArray.map(_.split(valueSeparator).map(_.toInt))
//    //println(na(0).mkString("\t"))
//    a += na(0)
//    colToSol += getSolI(nRow, nCol, a(a.length-1))
//    var solls = 0.0
//    for(j <- 0 until m){
//      solls += matrix(j/nCol)(j%nCol) * na(0)(j)
//    }
//    if(solls <= relax.getConstrByName("z").get(GRB.DoubleAttr.Pi) + 1E-2){
//      go = false
//      println("Stop the generation of columns")
//    }
//    val col = new GRBColumn()
//    var cc = 0.0
//    for (j <- 0 until m) {
//      col.addTerm(na(0)(j), sel(j))
//      cc += originalMatrix(j / nCol)(j % nCol) * na(0)(j)
//    }
//    c += cc
//    col.addTerm(1.0, sel(m))
//    x += model.addVar(0.0, 1.0, cc, GRB.BINARY, col, "x_" + nn)
//    model.update()
////    val ncp = a.length-1
////    for(i <- 0 until ncp){
////      if(a(i).sum > 0 && a(ncp).sum > 0) {
////        val nc: Array[(Int, Int)] = (a(i) zip a(ncp))
////        if (nc.map(x => Math.max(0, x._1 + x._2 - 1)).sum == 0) {
////          solls = 0.0
////          for(j <- 0 until m){
////            solls += matrix(j/nCol)(j%nCol) * na(0)(j)
////          }
////          if(solls > relax.getConstrByName("z").get(GRB.DoubleAttr.Pi)) {
////            nn += 1
////            val col2 = new GRBColumn()
////            a += nc.map(x => Math.min(1, x._1 + x._2))
////            val q = getSolI(nRow, nCol, a(i))
////            val q2 = getSolI(nRow, nCol, a(ncp))
////            colToSol += Array(q(0) ++ q2(0), q(1) ++ q2(1))
////            cc = 0.0
////            for (j <- 0 until m) {
////              col2.addTerm(a(a.length - 1)(j), sel(j))
////              cc += originalMatrix(j / nCol)(j % nCol) * a(a.length - 1)(j)
////            }
////            c += cc
////            col2.addTerm(1.0, sel(m))
////            x += model.addVar(0.0, 1.0, cc, GRB.BINARY, col2, "x_" + nn)
////            model.update()
////            println("New column added !")
////          }
////        }
////      }
////    }
//    System.out.println()
//  }
//
//  env.set(GRB.IntParam.LogToConsole, 0)
//  model.optimize
//  bestScore = model.get(GRB.DoubleAttr.ObjVal).toFloat
//  System.out.println("Obj: " + model.get(GRB.DoubleAttr.ObjVal))
//  requiredTime = System.nanoTime()-time
//  println(requiredTime)
//  res += model.get(GRB.DoubleAttr.ObjVal)
//  println("x = c("+(1 to res.length).mkString(",")+")")
//  println("y = c("+res.mkString(",")+")\nplot(x, y)")
//
//  println((0 until a.length).filter(i => x(i).get(GRB.DoubleAttr.X) != 0.0).map(i => x(i).get(GRB.DoubleAttr.X) + "\tScore: " + c(i) + "\tRows: " +colToSol(i)(0).mkString(",") + "\tCols: " +colToSol(i)(1).mkString(",")).mkString("\n"))
//  println()
////  println(Array.tabulate(a.length)(i => if (x(i).get(GRB.DoubleAttr.X) != 0.0) {
////    val s = getSol(nRow, nCol, a(i));
////    println(x(i).get(GRB.DoubleAttr.X) + "\tScore: " + c(i) + "\tRows: " + (0 until nRow).filter(x => s(0)(x) == 1).mkString(",") + "\tCols: " + (0 until nCol).filter(x => s(1)(x) == 1).mkString(","))
////  } else {
////    ""
////  }).mkString(""))
//
//  println(model.getConstrs().length)
//
//
//  // Dispose of model and environment
//  model.dispose
//  env.dispose
//
//  println("Terminated")
//  requiredTime = System.nanoTime()-time
//  completed = true
//  failed = false
//  //storeJSON()



  class StoreProgression extends GRBCallback {

    def callback() = {
      if(where == GRB.CB_MIPSOL){
        val obj: Double = getDoubleInfo(GRB.CB_MIPSOL_OBJ)


        bestScore = obj.toFloat
      }
    }

  }

  /***
    * Erases the file `path`
    * And stores an empty column (subm)
    *
    * @param nr number of rows
    * @param nc number of cols
    * @param path path to store the file
    */
  def getCols(nr: Int, nc: Int, path: String): Unit = {
    new FileSaver("", path, false)
    //new FileSaver(Array.tabulate(nr * nc)(i => 0).mkString("\t")+"\n", path, false)
    recursiveCols(Array.tabulate(nr)(i => 0), 0, Array.tabulate(nc)(j => 0), 0, path)
  }

  /**
    * Stores a column (subm) in the file as a boolean matrix
    *
    * @param row boolean vector of the row selection
    * @param col boolean vector of the column selection
    * @param path to the file containing the solution
    */
  def recursiveCols(row: Array[Int], prow: Int, col: Array[Int], pcol: Int, path: String): Unit = {
    new FileSaver(Array.tabulate(row.length)(i => Array.tabulate(col.length)(j => if (row(i) * col(j) == 1) 1 else 0).mkString("\t")).mkString("\t"), path, true)
  }
  def getSol(nr: Int, nc: Int, sol: Array[Int]): Array[Array[Int]] = {
    val rows = Array.tabulate(nr)(i => 0)
    val cols = Array.tabulate(nc)(j => 0)
    for(i <- 0 until nr){
      for(j <- 0 until nc){
        if(sol(i*nc+j) != 0.0){
          cols(j) = 1
          rows(i) = 1
        }
      }
    }
    Array(rows, cols)
  }
  def getSolI(nr: Int, nc: Int, sol: Array[Int]): Array[Array[Int]] = {
    val r = getSol(nr, nc, sol)
    Array((0 until nr).filter(x => r(0)(x) == 1).toArray, (0 until nc).filter(x => r(1)(x) == 1).toArray)
  }
  def importSol(size: Int, sol: Array[Int]): Array[Int] = {
    (0 until size).filter(x => sol(x) == 1).toArray
  }

}
